<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/test', function () {
    return "akku";
});

Route::post('emp_register','RegistrationController@emp_register');
Route::post('emp_register_step1','RegistrationController@emp_register_step1_personal_detail');
Route::post('emp_register_step2','RegistrationController@emp_register_step2_education');
Route::post('emp_register_step3','RegistrationController@emp_register_step3_skill');
Route::post('emp_register_step4','RegistrationController@emp_register_step4_experience');
Route::get('get_company', 'RegistrationController@get_company');

Route::post('com_register','RegistrationController@com_register');
Route::post('com_register_step1','RegistrationController@com_register_step1_info');
Route::post('com_register_step2','RegistrationController@com_register_step2_director_detail');
Route::post('com_register_step3','RegistrationController@com_register_step3_business_detail');

Route::post('login','RegistrationController@login');

Route::post('reset_password','RegistrationController@reset_password');
Route::post('forgot_password','RegistrationController@forgot_password');

Route::get('getdata/{id}/{role_id}','RegistrationController@getdata');
Route::post('edit_personal','EditProfileController@edit_personal');
Route::post('edit_education','EditProfileController@edit_education');
Route::post('edit_skill','EditProfileController@edit_skill');
Route::post('edit_experience','EditProfileController@edit_experience');

Route::post('upload_cv','EditProfileController@upload_cv');
Route::post('forceDownloadFile','EditProfileController@forceDownloadFile');

Route::post('edit_company','EditProfileController@edit_company_info');
Route::post('edit_director','EditProfileController@edit_director');

Route::post('upload_back_image','EditProfileController@upload_back_image');


Route::post('add_blog','BlogControlller@create');
Route::get('getblog','BlogControlller@show');
Route::get('userblog/{user_id}','BlogControlller@userblog');

Route::post('add_jobs','JobController@add_jobs');
Route::get('getjob/{user_id}','JobController@getjob');

Route::post('add_projects','JobController@add_projects');
Route::get('getprojects/{user_id}','JobController@getprojects');

Route::get('getemployeeuser/{id}/{edit_id}','RegistrationController@getemployeeuser');
Route::post('add_new_user','RegistrationController@add_new_user');
Route::get('getemployee/{id}','RegistrationController@getemployee');
Route::get('approve_user/{user_id}', 'RegistrationController@approve_user');
Route::get('selected_user/{id}','EditCompanyProfileController@selected_user');

Route::post('upload_company_back_image','EditCompanyProfileController@upload_company_back_image');
Route::get('delete_jobs/{id}', 'EditCompanyProfileController@delete_jobs');
Route::get('get_jobs/{id}', 'EditCompanyProfileController@get_jobs');
Route::post('edit_job','EditCompanyProfileController@edit_job');

Route::get('delete_projects/{id}', 'EditCompanyProfileController@delete_projects');
Route::get('get_projects/{id}', 'EditCompanyProfileController@get_projects');
Route::post('edit_projects','EditCompanyProfileController@edit_projects');
Route::post('add','EditCompanyProfileController@add');
Route::get('delete_assign_projects/{id}', 'EditCompanyProfileController@delete_assign_projects');

Route::get('delete_blog/{id}', 'BlogControlller@delete_blog');
Route::get('get_blog/{id}', 'BlogControlller@get_blog');
Route::post('edit_blog','BlogControlller@edit_blog');


Route::get('search_data','BlogControlller@search_data');
Route::post('search_user_data','RegistrationController@search_user_data');
Route::get('getcomprofile/{id}','RegistrationController@getcomprofile');
Route::get('get_des_emp_profile/{id}','RegistrationController@get_des_emp_profile');
Route::get('get_dev_emp_profile/{id}','RegistrationController@get_dev_emp_profile');
//Admin Dasboard
Route::post('admin_login','AdminDashboardContoller@login');
Route::post('admin_register','AdminDashboardContoller@admin_register');

Route::post('add_slide_show','AdminDashboardContoller@add_slide_show');
Route::get('get_slides','AdminDashboardContoller@get_slides');
Route::get('delete_slides/{id}', 'AdminDashboardContoller@delete_slides');
Route::get('get_slide/{id}', 'AdminDashboardContoller@get_slide');
Route::post('edit_slide','AdminDashboardContoller@edit_slide');
 
Route::post('add_testimonial','AdminDashboardContoller@add_testimonial');
Route::get('get_testimonial','AdminDashboardContoller@get_testimonial');
Route::get('delete_testimonial/{id}', 'AdminDashboardContoller@delete_testimonial');
Route::get('get_testi/{id}', 'AdminDashboardContoller@get_testi');
Route::post('edit_testimonial','AdminDashboardContoller@edit_testimonial');

Route::post('add_top_ranks','AdminDashboardContoller@add_top_ranks');
Route::get('get_top_ranks','AdminDashboardContoller@get_top_ranks');
Route::get('delete_top_ranks/{id}', 'AdminDashboardContoller@delete_top_ranks');

Route::get('get_users','AdminDashboardContoller@get_users');

Route::get('delete_users/{id}', 'AdminDashboardContoller@destroy_user');
Route::get('update_users/{id}', 'AdminDashboardContoller@update_users');
Route::post('edit_users', 'AdminDashboardContoller@edit_users');
Route::get('approve_company/{user_id}', 'AdminDashboardContoller@approve_company');