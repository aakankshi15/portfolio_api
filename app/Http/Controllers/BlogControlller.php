<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;

use App\User;
use DB;
use Session;
use Cookie;
use Auth;
use Response;

class BlogControlller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	 public function search_data()
    {
			 
	$users=DB::table('users')->select('*')->get();
	$myArray = ['users'=>$users];
	return response()->json($myArray);
   }
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
		 $user_id 		     = $request->post('user_id');
		 $post_title         = $request->post('post_title');
		 $post_description 	 = $request->post('post_description');
	     $author_name 		 = $request->post('author_name');
	   //  $post_date 		 = $request->post('post_date');
    	 $post_image 		 = $request->file('post_image');
		 $post_date = date("Y/m/d");
		
		$image = $request->file('post_image');
		$name = time() . '.' . $image->getClientOriginalExtension();
		$path = public_path('blog_image');
		
		if ( $image->move( $path, $name ) != false || ! is_null( $image->move( $path, $name ) ) ) {
			$file = "File Uploaded Successfully";
		}
		else {
			$file = "Error";
		}
			        DB::table('blogs')->insert(['user_id'=>$user_id,'post_title'=>$post_title,'post_description'=>$post_description,'author_name'=>$author_name,'post_image'=>$name,'post_date'=>$post_date]);
			
		  
			
			 $myArray = ['code'=>200];
                  return response()->json($myArray); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\odel  $odel
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
		 $server = $_SERVER['SERVER_NAME'];
         $blogs=DB::table('blogs')->select('*')->get();
		  $myArray = ['blogs'=>$blogs,'server'=>$server];
                  return response()->json($myArray);
    }
    
	public function userblog($user_id)
	{
		$userblogs=DB::table('blogs')->select('*')->where('user_id',$user_id)->get();
		
		 $myArray = ['userblog'=>$userblogs];
                  return response()->json($myArray);
	}
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\odel  $odel
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\odel  $odel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\odel  $odel
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
        //
    }
	
	
	
	
	    public function delete_blog($id){
			DB::table('blogs')->where('id',$id)->delete();
			 $myArray = ['msg'=>'successfully deleted'];
                  return response()->json($myArray);
		}
		public function get_blog($id)
        {
			
			  	$blog=DB::table('blogs')->select('*')->where('id',$id)->get();
			
			    $myArray = ['blog'=>$blog];
                  return response()->json($myArray);
		}
		public function edit_blog(Request $request)
		{                       
		    
              $id     =  $request->input('id');
              $post_title     =  $request->input('post_title');
			  $post_description     =  $request->input('post_description');
			  $author_name     =  $request->input('author_name');
			
			  $post_image     =  $request->input('post_image');
			
			  DB::table('blogs')->where('id',$id)->update(['post_title'=>$post_title,'post_description'=>$post_description,'author_name'=>$author_name,'post_image'=>$post_image]);
			   
			   $myArray = [ 'code'=>200,'msg'=>'Successfully Update'];
                  return response()->json($myArray);
		
		}
}
