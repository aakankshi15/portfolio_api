<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use DB;
use Session;
use Cookie;
use Auth;
use Response;

class EditProfileController extends Controller
{
     public function edit_personal(Request $request)
	  {
		 $getData = json_decode($request->data);
			  $user_id = $getData->user_id;
			  $employee_name = $getData->employee_name;
              $current_company = $getData->current_company;
			  $employee_dob =  $getData->employee_dob;
			  $contact_num= $getData->contact_num;
			  $employee_address =  $getData->employee_address;
			  $employee_gender =  $getData->employee_gender;

			  DB::table('emp_personal_details')->where('user_id',$user_id)->update(['employee_name'=>$employee_name,'current_company'=>$current_company,'employee_dob'=>$employee_dob,'contact_num'=>$contact_num,'employee_address'=>$employee_address,'employee_gender'=>$employee_gender]);
			  
			  $myArray = ['code'=>200, 'msg'=>'Successfully Update'];
                  return response()->json($myArray);
				  
			  
	  }
	  public function edit_education(Request $request)
	  {
		 
		 $getData = json_decode($request->data);
			  foreach($getData as $Data )
		      {
			        $id =$Data->id;  
                    $user_id =$Data->user_id;					
                    $course_id = $Data->course_id;
		            $institute_name= $Data->institute_name;		
		            $year_of_passing= $Data->year_of_passing;
                    $course_type = $Data->course_type;
			       if(empty($id)){
						 DB::table('emp_educations')->insert(['user_id'=>$user_id,'course_id'=>$course_id,'institute_name'=>$institute_name,'year_of_passing'=>$year_of_passing,'course_type'=>$course_type]);
					}
					else{
			        DB::table('emp_educations')->where('id',$id)->update(['course_id'=>$course_id,'institute_name'=>$institute_name,'year_of_passing'=>$year_of_passing,'course_type'=>$course_type]);
					}
			
		   }
			  
			  $myArray = ['code'=>200, 'msg'=>'Successfully Update'];
                  return response()->json($myArray);
				  
			  
	  }
	  
	   public function edit_skill(Request $request)
	  {
		
		 $getData = json_decode($request->data);
		
			  foreach($getData as $Data)
		      {
			        $id =$Data->id;
                    $user_id =$Data->user_id;					
                    $employee_skill = $Data->employee_skill;
		            $experience= $Data->skill_experience;		
		            if(empty($id)){
						
			        DB::table('emp_skills')->insert(['user_id'=>$user_id,'employee_skill'=>$employee_skill,'experience'=>$experience]);
					}
					else{
			
			        DB::table('emp_skills')->where('id',$id)->update(['employee_skill'=>$employee_skill,'experience'=>$experience]);
					}
			
		       }
			  
			  $myArray = ['code'=>200, 'msg'=>'Successfully Update'];
                  return response()->json($myArray);
				  
			  
	  }
	  public function edit_experience(Request $request)
	  {
		
		 $getData = json_decode($request->data);
		
			  foreach($getData as $Data)
		      {
			        $id =$Data->id;   
                    $user_id =$Data->user_id;  					
                    $company_Name = $Data->company_Name;
		            $position= $Data->position;		
		            $employee_id = $Data->employee_id;
				    $time_prefered = $Data->time_prefered;
				    $work_exp_year = $Data->work_exp_year;
				    $date_of_joining = $Data->date_of_joining;
			         
					 if(empty($id)){
						   DB::table('emp_experiences')->insert(['user_id'=>$user_id,'company_Name'=>$company_Name,'position'=>$position,'employee_id'=>$employee_id,'time_prefered'=>$time_prefered,'work_exp_year'=>$work_exp_year,'date_of_joining'=>$date_of_joining]);
					 }
					 else{
			        DB::table('emp_experiences')->where('id',$id)->update(['company_Name'=>$company_Name,'position'=>$position,'employee_id'=>$employee_id,'time_prefered'=>$time_prefered,'work_exp_year'=>$work_exp_year,'date_of_joining'=>$date_of_joining]);
					 }
			
		       }
			  
			  $myArray = ['code'=>200, 'msg'=>'Successfully Update'];
                  return response()->json($myArray);
				  
			  
	  }
	  public function upload_cv(Request $request){
		 
		  $user_id= $request->post('user_id');
		   $image = $request->file('resume');
	
		$name = time() . '.' . $image->getClientOriginalExtension();
		$path = public_path('resume');
		
		if ( $image->move( $path, $name ) != false || ! is_null( $image->move( $path, $name ) ) ) {
			$file = "File Uploaded Successfully";
		}
		else {
			$file = "Error";
		}
		  
		 DB::table('emp_personal_details')->where('user_id',$user_id)->update(['employee_resume'=>$name]);  
		 
		 $myArray = ['code'=>200, 'msg'=>'Successfully Update'];
                  return response()->json($myArray);
	  }
	  
	  public function forceDownloadFile(Request $request)
    {
		
         $user_id= $request->post('user_id');
		
        $file=DB::table('emp_personal_details')
            ->where('user_id', $user_id)
            ->select('*')
            ->get();
         $map_file= $file[0]->employee_resume;
          
          
       // $filePath = public_path($map_file);
         $filePath = public_path('resume').$map_file;

        $headers = ['Content-Type: application/png'];
        $fileName = time().'.pdf';
        return response()->download($filePath, $fileName, $headers);
    }
	
	public function upload_back_image(Request $request){
		 
		   $user_id= $request->post('user_id');
		   
      if($request->file('profile_image')){		 
		 $image = $request->file('profile_image');
	   	 $name = time() . '.' . $image->getClientOriginalExtension();
		 $path = public_path('images');
		
		  if ( $image->move( $path, $name ) != false || ! is_null( $image->move( $path, $name))) {
			$file = "File Uploaded Successfully";
		   }
		   else {
		 	$file = "Error";
		    }
		  
		   DB::table('emp_personal_details')->where('user_id',$user_id)->update(['employee_image'=>$name]);  
		 
		  $myArray = ['code'=>200, 'msg'=>'Successfully Update'];
                  return response()->json($myArray);
	  }
	  else 
	  {
		 $image = $request->file('background_image');
	   	 $name = time() . '.' . $image->getClientOriginalExtension();
		 $path = public_path('images');
		
		if ( $image->move( $path, $name ) != false || ! is_null( $image->move( $path, $name))) {
			$file = "File Uploaded Successfully";
		 }
		 else {
		 	$file = "Error";
		 }
		  
		 DB::table('emp_personal_details')->where('user_id',$user_id)->update(['back_image'=>$name]);  
		 
		 $myArray = ['code'=>200, 'msg'=>'Successfully Update'];
                  return response()->json($myArray);
	  }
}


//Compnay profile EDit
 public function edit_company_info(Request $request)
	  {
		 $getData = json_decode($request->data);
			  $user_id = $getData->user_id;
			  $company_name = $getData->company_name;
              $company_url = $getData->company_url;
			  $country =  $getData->country;
			  $state= $getData->state;
			  $city =  $getData->city;
			  $postal_code =  $getData->postal_code;

			  DB::table('com_infos')->where('company_user_id',$user_id)->update(['company_name'=>$company_name,'company_url'=>$company_url,'country'=>$country,'state'=>$state,'city'=>$city,'postal_code'=>$postal_code]);
			  
			  $myArray = ['code'=>200, 'msg'=>'Successfully Update'];
                  return response()->json($myArray);
				  
			  
	  }
	  public function edit_director(Request $request)
	  {
		 
		 $getData = json_decode($request->data);
			  foreach($getData as $Data )
		      {
			        $id =$Data->id;  
                    $user_id=$Data->user_id; 					
                    $first_name = $Data->first_name;
		            $last_name= $Data->last_name;		
		            $email= $Data->email;
                    $contact = $Data->contact;
                    $address = $Data->address;	
					
					if(empty($id)){
						
						  DB::table('com_director_details')->insert(['company_user_id'=>$user_id,'first_name'=>$first_name,'last_name'=>$last_name,'email'=>$email,'contact'=>$contact,'address'=>$address]);
					}
					else{
						 DB::table('com_director_details')->where('id',$id)->update(['first_name'=>$first_name,'last_name'=>$last_name,'email'=>$email,'contact'=>$contact,'address'=>$address]);
					}
			       
			
		   }
			  
			  $myArray = ['code'=>200, 'msg'=>'Successfully Update'];
                  return response()->json($myArray);
				  
			  
	  }



}
