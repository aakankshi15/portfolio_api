<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use DB;
use Session;
use Cookie;
use Auth;
use Response;


 
class AdminDashboardContoller extends Controller
{
     public function login(Request $request)
        {
			
			
			 $email=$request->input('email');
		     $password = $request->input('password');
			
			if (Auth::attempt(['email' => $email, 'password' => $password]))
				{
				 $userdata=Auth::user();
				 $token=$userdata->createToken('MyApp')->accessToken;
				 $authId=$userdata->user_id;
				 $role_id=$userdata->role_id;
			 
					  $myArray = ['code'=>200, 'msg'=>'Login SuccessFully','user_id'=>$authId,'role_id'=>$role_id,'token'=>$token,'email'=>$email];
                      return response()->json($myArray);
				}
           
			else 
		    	{
				 $myArray = ['code'=>201, 'token'=>$token, 'msg'=>'Login Failed Wrong Data Passed','email'=>$email];
                  return response()->json($myArray);
             
               }
		}
		
		public function admin_register(Request $request)
        {
			
			 $email     =  $request->input('email');
			 $phone_no  =  $request->input('phone_number');
		     $user_id   =  date('YmdHis');
			 $passwordd =  $request->input('password');
			 $password  =  bcrypt($passwordd);
			 $role_id   = '3';
			  
			  DB::table('users')->insert(['user_id'=>$user_id,'email'=>$email,'phone_no'=>$phone_no,'password'=>$password,'role_id'=>$role_id]);
			  
			 $myArray = ['response_code'=>200, 'msg'=>'Register Successfully'];
                  return response()->json($myArray);
		
		}
		public function add_slide_show(Request $request)
        {
			
			 $title     =  $request->input('title');
		     $image = $request->file('slider_image');
	     	 $name = time() . '.' . $image->getClientOriginalExtension();
		     $path = public_path('images');
		
		      if ( $image->move( $path, $name ) != false || ! is_null( $image->move( $path, $name))) {
		    	$file = "File Uploaded Successfully";
		      }
		      else {
		     	$file = "Error";
		      }
			  
			  DB::table('admin_slide_show')->insert(['title'=>$title,'slider_image'=>$name]);
			  	$slides=DB::table('admin_slide_show')->select('*')->get();
			  
			 $myArray = ['code'=>200, 'msg'=>'Register Successfully','slide'=>$slides];
                  return response()->json($myArray);
		
		}
		public function get_slides(Request $request)
        {
			 
			  	$slides=DB::table('admin_slide_show')->select('*')->get();
			    $testimonials=DB::table('admin_testimonials')->select('*')->get();
			    $myArray = ['slide'=>$slides,'testimonials'=>$testimonials];
                  return response()->json($myArray);
		}
		public function delete_slides($id){
			DB::table('admin_slide_show')->where('id',$id)->delete();
			 $myArray = ['msg'=>'successfully deleted'];
                  return response()->json($myArray);
		}
		
		
		public function get_slide($id)
        {
			 
			  	$slides=DB::table('admin_slide_show')->where('id',$id)->select('*')->get();
			    $myArray = ['slide'=>$slides];
                  return response()->json($myArray);
		}
		
		public function edit_slide(Request $request){
		   $title = $request->post('title');
	   	   $id = $request->post('id');
		   
	   	   $image = $request->file('slider_image');
		   
		     if(!($image)){
		       DB::table('admin_slide_show')->where('id',$id)->update(['title'=>$title]);  
		      }
		      else{
				   $imagename = time() . '.' . $image->getClientOriginalExtension();
		           $path = public_path('images');
                   if ( $image->move( $path, $imagename ) != false || ! is_null( $image->move( $path, $imagename ) ) )
				   {
			          $file = "File Uploaded Successfully";
		           }
		           else 
				   {
			           $file = "Error";
		           }
				   
		           DB::table('admin_slide_show')->where('id',$id)->update(['title'=>$title,'slider_image'=>$imagename]);  
		      }
		 
		  $myArray = ['code'=>200, 'msg'=>'Successfully Update'];
                  return response()->json($myArray);
		}
		
		
		
		public function add_testimonial(Request $request)
        {
			
			 $testiname     =  $request->input('name');
			 $description     =  $request->input('description');
		     $image = $request->file('image');
	     	 $name = time() . '.' . $image->getClientOriginalExtension();
		     $path = public_path('images');
		
		      if ( $image->move( $path, $name ) != false || ! is_null( $image->move( $path, $name))) {
		    	$file = "File Uploaded Successfully";
		      }
		      else {
		     	$file = "Error";
		      }
			  
			  DB::table('admin_testimonials')->insert(['name'=>$testiname,'image'=>$name,'description'=>$description]);
			  
			 $myArray = ['code'=>200, 'msg'=>'Register Successfully'];
                  return response()->json($myArray);
		
		}
		public function get_testimonial(Request $request)
        {
			 
			  	$testimonial=DB::table('admin_testimonials')->select('*')->get();
			    $myArray = ['testimonial'=>$testimonial];
                  return response()->json($myArray);
		}
		public function delete_testimonial($id){
			DB::table('admin_testimonials')->where('id',$id)->delete();
			 $myArray = ['msg'=>'successfully deleted'];
                  return response()->json($myArray);
		}
		
		public function get_testi($id)
        {
			 
			  	$testi=DB::table('admin_testimonials')->where('id',$id)->select('*')->get();
			    $myArray = ['testi'=>$testi];
                  return response()->json($myArray);
		}
		
		public function edit_testimonial(Request $request){
		    $name = $request->post('name');
		    $description = $request->post('description');
	   	    $id = $request->post('id');
       	    
			$image = $request->file('image');
		  
		  if(!($image)){
		       DB::table('admin_testimonials')->where('id',$id)->update(['name'=>$name,'description'=>$description]);  
		      }
		  else{
				   $imagename = time() . '.' . $image->getClientOriginalExtension();
		           $path = public_path('images');
                   if ( $image->move( $path, $imagename ) != false || ! is_null( $image->move( $path, $imagename ) ) )
				   {
			          $file = "File Uploaded Successfully";
		           }
		           else 
				   {
			           $file = "Error";
		           }
				   
		         DB::table('admin_testimonials')->where('id',$id)->update(['name'=>$name,'description'=>$description,'image'=>$imagename]);  
		      }
		  
		 
		  $myArray = ['code'=>200, 'msg'=>'Successfully Update'];
                  return response()->json($myArray);
		}
		
		
		
		
		
		
		
		public function add_top_ranks(Request $request)
        {
			
			 $name     =  $request->input('name');
			 $description     =  $request->input('description');
		     $image = $request->file('image');
	     	 $name = time() . '.' . $image->getClientOriginalExtension();
		     $path = public_path('images');
		
		      if ( $image->move( $path, $name ) != false || ! is_null( $image->move( $path, $name))) {
		    	$file = "File Uploaded Successfully";
		      }
		      else {
		     	$file = "Error";
		      }
			  
			  DB::table('admin_top_ranks')->insert(['name'=>$name,'image'=>$name,'description'=>$description]);
			
			  
			 $myArray = ['code'=>200, 'msg'=>'Register Successfully'];
                  return response()->json($myArray);
		
		}
		public function get_top_ranks(Request $request)
        {
			 
			  	$top_ranks=DB::table('admin_top_ranks')->select('*')->get();
			    $myArray = ['top_ranks'=>$top_ranks];
                  return response()->json($myArray);
		}
		public function delete_top_ranks($id){
			DB::table('admin_top_ranks')->where('id',$id)->delete();
			 $myArray = ['msg'=>'successfully deleted'];
                  return response()->json($myArray);
		}
		
		
		
		public function get_users(Request $request)
        {
			 
			  	$employee=DB::table('users')->where('role_id','1')->select('*')->get();
			    $company=DB::table('users')->where('role_id','2')->select('*')->get();
			    $myArray = ['employee'=>$employee,'company'=>$company];
                  return response()->json($myArray);
		}
		public function destroy_user($id){
			DB::table('users')->where('user_id',$id)->delete();
			 $myArray = ['msg'=>'successfully deleted'];
                  return response()->json($myArray);
		}
		public function update_users($id){
			$user = DB::table('users')->select('*')->where('user_id',$id)->get();
			
			
			 $myArray = ['user'=>$user];
                  return response()->json($myArray);
		}
		public function edit_users(Request $request){
		   $email = $request->post('email');
	   	   $phone_no = $request->post('phone_no');
	   	   $user_id = $request->post('user_id');
	   	   
		   DB::table('users')->where('user_id',$user_id)->update(['email'=>$email,'phone_no'=>$phone_no]);  
		 
		  $myArray = ['code'=>200, 'msg'=>'Successfully Update'];
                  return response()->json($myArray);
		}
	   public function approve_company($user_id){
		 
			$approve_id=DB::table('users')->where('user_id',$user_id)->select('*')->get();
			$data= $approve_id[0]->approve_user;
			if($data=='1'){	
			$approve_user = '0';
			}
			else{
				$approve_user = '1';
			}
		
			DB::table('users')->where('user_id',$user_id)->update(['approve_user'=>$approve_user]);
			 $myArray = ['msg'=>'Approved User'];
                  return response()->json($myArray);
		}
		
			
			
}
