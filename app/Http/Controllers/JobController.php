<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use DB;
use Session;
use Cookie;
use Auth;
use Response;

class JobController extends Controller
{
    public function add_jobs(Request $request)
	{
	
	 $getData = json_decode($request->data);
			  $company_id = $getData->company_id	;
			  $category = $getData->category;
              $designation = $getData->designation;
			  $description= $getData->description;
			  $eligibility =  $getData->eligibility;
			  $location = $getData->location;
			  $skill_required= $getData->skill_required;
			  $salary =  $getData->salary;
			  $experience =  $getData->experience;
			  
			  DB::table('company_jobs')->insert(['company_id'=>$company_id,'category'=>$category,'designation'=>$designation,'description'=>$description,'eligibility'=>$eligibility,'location'=>$location,'skill_required'=>$skill_required,'salary'=>$salary,'experience'=>$experience]);
			  
			   $myArray = ['code'=>200, 'MSG'=>'SUCCESS'];
                  return response()->json($myArray);
      }
	  
	  public function getjob($user_id)
	{
		 $server = $_SERVER['SERVER_NAME'];
		$newjobs=DB::table('company_jobs')->select('*')->where('company_id',$user_id)->get();
		
		$newprojects=DB::table('company_projects')->select('*')->where('company_id',$user_id)->get();
		
		$myArray = ['newjobs'=>$newjobs,'newprojects'=>$newprojects,'server'=>$server];
                  return response()->json($myArray);
	}
	
	 public function add_projects(Request $request)
	{
		
		 $server = $_SERVER['SERVER_NAME'];
		
		 $project_name 		 = $request->post('project_name');
	     $project_description = $request->post('project_description');
		 $company_id 		 = $request->post('company_id');
	     $project_image 		 = $request->file('project_image');
		
		$image = $request->file('project_image');
		$name = time() . '.' . $image->getClientOriginalExtension();
		$path = public_path('images');
		
		if ( $image->move( $path, $name ) != false || ! is_null( $image->move( $path, $name ) ) ) {
			$file = "File Uploaded Successfully";
		}
		else {
			$file = "Error";
		}
		
		 DB::table('company_projects')->insert(['company_id'=>$company_id,'project_name'=>$project_name,'project_description'=>$project_description,'project_images'=>$name]);
			  
			   $myArray = ['code'=>200, 'MSG'=>'SUCCESS' ,'server'=>$server];
                  return response()->json($myArray);
	    }
		/*
		$name 	  = $_FILES['project_image']['name'];
		$tmp_name = $_FILES['project_image']['tmp_name'];
		$size 	  = $_FILES['project_image']['size'];
		
		$image_name = public_path('images') . '/a.png';
		
		$run = move_uploaded_file( $tmp_name, $image_name );
		
		if ( $run ) {
			echo "Ok";
		}
		else {
			echo "Not";
		}
		*/
		
		
		
		
		
		#echo $image->getClientOriginalExtension();
		
		#$image->move($image_name , 'a.png');
        #$this->save();
		
		/* exit;
		$getData = json_decode($request->fd);
		
		print_r($getData);
		die();
		
		echo  $getData->project_image;
		die(); */
			/*
		if ( move_uploaded_file( $getData->project_images, public_path('images') . '/a.png' ) ) {
			
			echo "File Uploaded successfully";
		}
	else {
		echo  $getData->project_images;
	}
		
		  $project_images= $getData->project_images;
		  $a = move_uploaded_file( $project_images, 'a.png' );
		  
		  
		  
		 //$project_images = $project_images->getClientOriginalName();
             //$getData->project_images->move(public_path('images'),$project_images); 
		die();
		
	print_r($request); */
	/*  $getData = json_decode($request->data);
			  $company_id = $getData->company_id	;
			  $project_name = $getData->project_name;
              $project_description = $getData->project_description;
			  $project_images= $getData->project_images;
			 
			  
			  DB::table('company_projects')->insert(['company_id'=>$company_id,'project_name'=>$project_name,'project_description'=>$project_description,'project_images'=>$project_images]);
			  
			   $myArray = ['code'=>200, 'MSG'=>'SUCCESS'];
                  return response()->json($myArray);
      } */
	  
	  public function getprojects($user_id)
	{
		$newprojects=DB::table('company_projects')->select('*')->where('company_id',$user_id)->get();
		
		 $myArray = ['newprojects'=>$newprojects];
                  return response()->json($myArray);
	}
}
