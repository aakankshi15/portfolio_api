<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use DB;
use Session;
use Cookie;
use Auth;
use Response;

class RegistrationController extends Controller
{
 public function emp_register(Request $request)
        {
		try{
			 $getData = json_decode($request->data);
			  $user_id = $getData->user_id;
			  $email = $getData->email;
              $phone = $getData->phone;
			  $passwordd= $getData->password;
			  $password=bcrypt($passwordd);
			  $role_id = '1';
			  
			  DB::table('users')->insert(['user_id'=>$user_id,'email'=>$email,'phone_no'=>$phone,'password'=>$password,'role_id'=>$role_id]);
		$rndno=rand(1000,9999);
		
			 /*    $mobile=$phone;
				$senderId="Aakankshi";
				$rndno=rand(1000,9999);
				$message=urlencode("test otp number:".$rndno);
				$route="route=4";
				$authkey="213315AtaiOwL5FF5ae8a946";
				$postData=array(
				'authkey'=>$authkey,
				'mobiles'=>$mobile,
				'message'=>$message,
				'sender'=>$senderId,
				'route'=>$route);
				$url="https://control.msg91.com/api/sendhttp.php";

				$ch=curl_init();
				curl_setopt_array($ch,array(
				CURLOPT_URL=>$url,
				CURLOPT_RETURNTRANSFER=>true,
				CURLOPT_POST=>true,
				CURLOPT_POSTFIELDS=>$postData
				));
				//curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,0);
				//curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,0);
				$output=curl_exec($ch);
				if(curl_errno($ch)){
					echo 'error:'.curl_error($ch);
				}
				curl_close($ch);  */
		}
		catch(Exception $e){
		    echo $e->getMessage();
		}
			
			 $myArray = ['code'=>200, 'name'=>'HD','otp'=>$rndno];
                  return response()->json($myArray);
				  
				  
		    
   }
   
   public function emp_register_step1_personal_detail(Request $request)
        {
		try{	
	    	
		   $user_id = $request->post('user_id');
			//$user_id=6536445435564356;
		    $employee_name = $request->post('employee_name');
			$current_company = $request->post('current_company');
		    $doj = $request->post('date_of_joining');
			 $jodd = substr($doj,4,11);
            $date_of_joining =date('m/d/Y', strtotime($jodd));
			
			$dob = $request->post('employee_dob');
		    $strDate = substr($dob,4,11);
            $employee_dob =date('m/d/Y', strtotime($strDate));
	
		    $employee_pincode = $request->post('employee_pincode');
			$contact_num = $request->post('contact_num');
		    $employee_address = $request->post('employee_address');
			$employee_gender = $request->post('employee_gender');
			$company_id = $request->post('company_id');
			$position = $request->post('position');
		    $employee_image 		 = $request->file('employee_image');
		
		$image = $request->file('employee_image');
		$name = time() . '.' . $image->getClientOriginalExtension();
		$path = public_path('images');
		
		if ( $image->move( $path, $name ) != false || ! is_null( $image->move( $path, $name ) ) ) {
			$file = "File Uploaded Successfully";
		}
		else {
			$file = "Error";
		}
		
			  
			  DB::table('emp_personal_details')->insert(['user_id'=>$user_id,'company_id'=>$company_id,'employee_name'=>$employee_name,'current_company'=>$current_company,'date_of_joining'=>$date_of_joining,'employee_dob'=>$employee_dob,'employee_pincode'=>$employee_pincode,'contact_num'=>$contact_num,'employee_address'=>$employee_address,'employee_gender'=>$employee_gender,'position'=>$position,'employee_image'=>$name]);
			  
 		  
		}
		catch(Exception $e){
		        echo $e->getMessage();
		}
			
			 $myArray = ['code'=>200, 'name'=>'HD'];
                  return response()->json($myArray);
				  
				 
				  
   }
   
   public function get_company(Request $request)
        {
			    
				
			  	$company =DB::table('com_infos')
				          ->join('users','users.user_id','com_infos.company_user_id')
						  ->where('users.approve_user','1')
						  ->select('*')->get();
				

			    $myArray = ['company'=>$company];
                  return response()->json($myArray);
		}
    	
    public function emp_register_step2_education(Request $request)
	{
		
        	$getData = json_decode($request->data);
			  foreach($getData as $Data )
		      {
			        $user_id =$Data->user_id;    
                    $course_id = $Data->course_id;
		            $institute_name= $Data->institute_name;		
		            $yop= $Data->year_of_passing;
					$strDate = substr($yop,4,11);
                    $year_of_passing =date('m/d/Y', strtotime($strDate));
                    $course_type = $Data->course_type;
			
			        DB::table('emp_educations')->insert(['user_id'=>$user_id,'course_id'=>$course_id,'institute_name'=>$institute_name,'year_of_passing'=>$year_of_passing,'course_type'=>$course_type]);
			
		   }
			
			 $myArray = ['code'=>200, 'name'=>'HD'];
                  return response()->json($myArray);
	}
	
	 public function emp_register_step3_skill(Request $request)
	 {
		
		 
        	$getData = json_decode($request->data);
	        foreach($getData as $Data )
		    {
				$user_id =$Data->user_id;   
			    $employee_skill= $Data->employee_skill;
			    $experience=$Data->experience;
			    DB::table('emp_skills')->insert(['user_id'=>$user_id,'employee_skill'=>$employee_skill,'experience'=>$experience]);
		     }
			
			 $myArray = ['code'=>200, 'name'=>'HD'];
                  return response()->json($myArray);
      	}
		
	public function emp_register_step4_experience(Request $request)
	{
		
		
        	$getData = json_decode($request->data);
			
			  foreach($getData as $Data )
		      {
			        $user_id =$Data->user_id; 
                    $company_Name = $Data->company_Name;
		            $employee_id= $Data->employee_id;		
		            $position= $Data->position;
                    $time_prefered = $Data->time_prefered;
					$work_exp_year = $Data->work_exp_year;
		            $work_exp_month= $Data->work_exp_month;		
		            $doj= $Data->date_of_joining;
				    $dojstr = substr($doj,4,11);
                    $date_of_joining =date('m/d/Y', strtotime($dojstr));
					
                    $doc= $Data->date_of_closing;
						$docstr = substr($doc,4,11);
                    $date_of_closing =date('m/d/Y', strtotime($docstr));
					
			DB::table('emp_experiences')->insert(['user_id'=>$user_id,'employee_id'=>$employee_id,'company_Name'=>$company_Name,'position'=>$position,'time_prefered'=>$time_prefered,'work_exp_year'=>$work_exp_year,'work_exp_month'=>$work_exp_month,'date_of_joining'=>$date_of_joining,'date_of_closing'=>$date_of_closing ]);
			
			$status = '1';
			DB::table('users')->where('user_id',$user_id)->update(['status'=>$status]);
			
		   }
			
			 $myArray = ['code'=>200, 'name'=>'HD'];
                  return response()->json($myArray);
	}
	
	
	
 /*  company registration */
	
	  public function com_register(Request $request)
        {
		try{		
	          $getData = json_decode($request->data);
			  $company_user_id = $getData->company_user_id;
			  $email = $getData->email;
              $phone = $getData->phone;
			   $passwordd= $getData->password;
			  $password=bcrypt($passwordd);
			  $role_id = '2';
			  $rndno=rand(1000,9999);
			   DB::table('users')->insert(['user_id'=>$company_user_id,'email'=>$email,'phone_no'=>$phone,'password'=>$password,'role_id'=>$role_id]);
	
	    	/* 	$mobile=$phone;
				$senderId="Aakankshi";
				$rndno=rand(1000,9999);
				
				$message=urlencode("test otp number:".$rndno);
				$route="route=4";
				$authkey="213315AtaiOwL5FF5ae8a946";
				$postData=array(
				'authkey'=>$authkey,
				'mobiles'=>$mobile,
				'message'=>$message,
				'sender'=>$senderId,
				'route'=>$route);
				$url="https://control.msg91.com/api/sendhttp.php";

				$ch=curl_init();
				curl_setopt_array($ch,array(
				CURLOPT_URL=>$url,
				CURLOPT_RETURNTRANSFER=>true,
				CURLOPT_POST=>true,
				CURLOPT_POSTFIELDS=>$postData
				));
				
				$output=curl_exec($ch);
				if(curl_errno($ch)){
					echo 'error:'.curl_error($ch);
				}
				curl_close($ch); */
       	
		}
		catch(Exception $e){
		    echo $e->getMessage();
		}
			 
			
			 $myArray = ['code'=>200, 'name'=>'HD' ,'otp'=>$rndno];
                  return response()->json($myArray);
				  
		  
   }
	
	  
    public function com_register_step1_info(Request $request)
        {
		try{	
		  
			$company_user_id = $request->post('company_user_id');
		    $company_name = $request->post('company_name');
			$company_id = $request->post('company_id');
		    $company_url = $request->post('company_url');
			$country = $request->post('country');
		    $state = $request->post('state');
			$city = $request->post('city');
		    $postal_code = $request->post('postal_code');
			
		    $company_logo 		 = $request->file('company_logo');
		
		$image = $request->file('company_logo');
		$name = time() . '.' . $image->getClientOriginalExtension();
		$path = public_path('images');
		
		if ( $image->move( $path, $name ) != false || ! is_null( $image->move( $path, $name ) ) ) {
			$file = "File Uploaded Successfully";
		}
		else {
			$file = "Error";
		}
		 
			  DB::table('com_infos')->insert(['company_user_id'=>$company_user_id,'company_name'=>$company_name,'company_id'=>$company_id,'company_url'=>$company_url,'company_logo'=>$name,'country'=>$country,'state'=>$state,'city'=>$city,'postal_code'=>$postal_code]);
		 
		}
		catch(Exception $e){
		        echo $e->getMessage();
		}
			
			 $myArray = ['code'=>200, 'name'=>'HD'];
                  return response()->json($myArray);
				  
		 
   }
   
     public function com_register_step2_director_detail(Request $request)
	 {
		
		  
			$getData = json_decode($request->data);
			
			  foreach($getData as $Data )
		      { 	 
			        $company_user_id=$Data->company_user_id;
                    $first_name = $Data->first_name;
		            $last_name= $Data->last_name;		
		            $email= $Data->email;
                    $contact = $Data->contact;
					$address=$Data->address;
			DB::table('com_director_details')->insert(['company_user_id'=>$company_user_id,'first_name'=>$first_name,'last_name'=>$last_name,'email'=>$email,'contact'=>$contact,'address'=>$address]);
			
		   }
			
			 $myArray = ['code'=>200, 'name'=>'HD'];
                  return response()->json($myArray);
	}
	
	public function com_register_step3_business_detail(Request $request)
        {
			$getData = json_decode($request->data);
			
			  $company_user_id = $getData->company_user_id;
			  $business_type = $getData->activity_type;
              $business_detail = $getData->activity_detail;
			  DB::table('com_business_details')->insert(['company_user_id'=>$company_user_id,'business_type'=>$business_type,'business_detail'=>$business_detail]);
			
		   
			
			 $myArray = ['code'=>200, 'name'=>'HD'];
                  return response()->json($myArray);
   }
   public function login(Request $request)
        {
			 $email=$request->input('email');
		     $password = $request->input('password');
			
			if (Auth::attempt(['email' => $email, 'password' => $password])) {
				 $userdata=Auth::user();
				 $token=$userdata->createToken('MyApp')->accessToken;
			     $authId=$userdata->user_id;
		    	 $role_id=$userdata->role_id;
				
			    if ($role_id=='1'){
			         $expe=DB::table('emp_personal_details')->select('*')->where('user_id',$authId)->get();
					 $position=$expe[0]->position;
					
			         if($position=='Web Designer'){
				      $myArray = ['code'=>202, 'msg'=>'Login SuccessFull','user_id'=>$authId,'role_id'=>$role_id,'token'=>$token];
                      return response()->json($myArray);
				     }
				     else{
					  $myArray = ['code'=>200, 'msg'=>'Login SuccessFull','user_id'=>$authId,'role_id'=>$role_id,'token'=>$token];
                      return response()->json($myArray);
					 }
				}
				else{
					  $companyData=DB::table('com_infos')->select('*')->where('company_user_id',$authId)->get();
					  $company_name = $companyData[0]->company_name;
					  $myArray = ['code'=>200, 'msg'=>'Login SuccessFull','company_name'=>$company_name,'user_id'=>$authId,'role_id'=>$role_id,'token'=>$token];
                      return response()->json($myArray);
				}
           
            }
			else 
			{
				 $myArray = ['code'=>201, 'msg'=>'Login Failed Wrong Data Passed'];
                  return response()->json($myArray);
             
              }
		}
   
      public function getdata($id,$role_id)
	  {
		 
		   $server = $_SERVER['SERVER_NAME'];
		  if($role_id=='1')
		  {
                     $expe=DB::table('emp_personal_details')->select('*')->where('user_id',$id)->get();
			
			         $position=$expe[0]->position;
			
         
				    if($role_id=='1' && $position=='Web Designer'){
					   $users=DB::table('users')->select('*')->where('user_id',$id)->get();
					   $edu=DB::table('emp_educations')->select('*')->where('user_id',$id)->get();
					   $personal=DB::table('emp_personal_details')->select('*')->where('user_id',$id)->get();
					   $expe=DB::table('emp_experiences')->select('*')->where('user_id',$id)->get();
					   $skill=DB::table('emp_skills')->select('*')->where('user_id',$id)->get();
                         $approve='1';					    
						$projects= DB::table('project-assign-user')
                                  ->join('company_projects','company_projects.id','project-assign-user.project_id')	
								  ->join('users','users.user_id','project-assign-user.user_id')	
                                  ->where(['project-assign-user.user_id'=>$id,'users.approve_user'=>$approve])
								  ->select('*')								  
					              ->get(); 
					  
					  
					 
			
					  $myArray = ['users'=>$users,'server'=>$server, 'education'=>$edu,'personal'=>$personal,'expe'=>$expe,'skill'=>$skill,'projects'=>$projects];
								  return response()->json($myArray);
					 
					
				     }
				     else {
					
					   $users=DB::table('users')->select('*')->where('user_id',$id)->get();
					   $edu=DB::table('emp_educations')->select('*')->where('user_id',$id)->get();
					   $personal=DB::table('emp_personal_details')->select('*')->where('user_id',$id)->get();
					   $expe=DB::table('emp_experiences')->select('*')->where('user_id',$id)->get();
					   $skill=DB::table('emp_skills')->select('*')->where('user_id',$id)->get();
					   $approve='1';
					   $projects= DB::table('project-assign-user')
                                  ->join('company_projects','company_projects.id','project-assign-user.project_id')	
								  ->join('users','users.user_id','project-assign-user.user_id')	
                                  ->where(['project-assign-user.user_id'=>$id,'users.approve_user'=>$approve])
								  ->select('*')								  
					              ->get(); 
					  
					  
					   $myArray = ['users'=>$users,'server'=>$server, 'education'=>$edu,'personal'=>$personal,'expe'=>$expe,'skill'=>$skill,'projects'=>$projects];
								  return response()->json($myArray);
				    }
				
				 

		  }

		else{
					  
					  
					   $users=DB::table('users')->select('*')->where('user_id',$id)->get();
					   $companyinfo=DB::table('com_infos')->select('*')->where('company_user_id',$id)->get();
					   $directors=DB::table('com_director_details')->select('*')->where('company_user_id',$id)->get();
					   $business=DB::table('com_business_details')->select('*')->where('company_user_id',$id)->get();
					   
					  
					  
					   $myArray = ['users'=>$users, 'server'=>$server,'companyinfo'=>$companyinfo,'directors'=>$directors,'business'=>$business];
								  return response()->json($myArray);
					
		   }
						
		
	            

             
       }   
	   
	   
	   
	   public function add_new_user(Request $request)
        {
		try{
			
			  $getData = json_decode($request->data);
			  $user_id = $getData->user_id;
			  $email = $getData->email;
              $phone = $getData->phone_no;
			  $passwordd= $getData->password;
			  $password=bcrypt($passwordd);
			  $role_id = '1';
			  $status = '1';
			   $employee_name = $getData->employee_name;
			   $current_company = $getData->current_company;
               $date_of_joining = $getData->date_of_joining;
			   $employee_dob= $getData->employee_dob;
			   $employee_pincode = $getData->employee_pincode;
			   //$contact_num = $getData->contact_num;
               $employee_address = $getData->employee_address;
			   $employee_gender= $getData->employee_gender;
			   $company_id = $getData->company_id;
			   $position = $getData->position;
			  
			  DB::table('users')->insert(['user_id'=>$user_id,'email'=>$email,'phone_no'=>$phone,'password'=>$password,'role_id'=>$role_id,'status'=>$status]);
		      DB::table('emp_personal_details')->insert(['user_id'=>$user_id,'employee_name'=>$employee_name,'current_company'=>$current_company,'date_of_joining'=>$date_of_joining,'employee_dob'=>$employee_dob,'employee_pincode'=>$employee_pincode,'contact_num'=>$phone,'employee_address'=>$employee_address,'employee_gender'=>$employee_gender,'company_id'=>$company_id,'position'=>$position]);
		
		
		}
		catch(Exception $e){
		    echo $e->getMessage();
		}
			
			 $myArray = ['code'=>200,'msg'=>'Successfully add new user'];
                  return response()->json($myArray);
				  			  	    
       }
   
     public function getemployee($company_id)
	  {
		  
		  $empData=DB::table('emp_personal_details')->select('*')->where('company_id',$company_id)->get();
		
		  $key=array();
		   foreach($empData as $Data )
		   {
			   array_push($key,$Data->user_id);
			  $empList = DB::table('users')
			         ->join('emp_personal_details', 'emp_personal_details.user_id', 'users.user_id')
                     ->whereIn('users.user_id', $key)
					 ->select('*')
                     ->get(); 
			
			 
		   } 
			
		  
	       $myArray = ['code'=>200,'empList'=> $empList];
                  return response()->json($myArray);
	  }
	  
	   public function  getemployeeuser($company_id,$project_id)
	  {  
	      $pro =DB::table('project-assign-user')->select('*')->where('project_id',$project_id)->get();	
		  $approve_status = '1';
		  if(count($pro) == 0){
			  $empList=DB::table('emp_personal_details')
			           ->join('users', 'users.user_id', 'emp_personal_details.user_id')
			           ->select('*')
					   ->where('emp_personal_details.company_id',$company_id)
					   ->where('users.approve_user',$approve_status)
					   ->get();
		  }
		  else
		  {	
          $key=array();		  
		  foreach($pro as $Data )
		   {
			   array_push($key,$Data->user_id);
			   $empList=DB::table('emp_personal_details')
			           ->select('*')
					   ->where('company_id',$company_id)
					   ->whereNotIn('user_id',$key)
					   ->get();
					   
						
		   }
		  }
		
		  
	       $myArray = ['code'=>200,'empList'=> $empList];
                  return response()->json($myArray);
	  }
	  
	 
	  public function approve_user($user_id){
		 
		$approve_id=DB::table('users')->where('user_id',$user_id)->select('*')->get();
		$data= $approve_id[0]->approve_user;
		if($data=='1'){	
		   $approve_user = '0';
		}
		else{
			$approve_user = '1';
		}
		
			DB::table('users')->where('user_id',$user_id)->update(['approve_user'=>$approve_user]);
			 $myArray = ['msg'=>'Approved User'];
                  return response()->json($myArray);
		}
   
   public function reset_password(Request $request)
        {
		try{
			 
			 
			  $email = $request->post('email');
			  $userdata=DB::table('users')->select('*')->where('email',$email)->get();
              $phone_no = $userdata[0]->phone_no; 			  
			  $user_id = $userdata[0]->user_id; 
		
		
			    $mobile=$phone_no;
				$senderId="Aakankshi";
				$rndno=rand(1000,9999);
				$message=urlencode("test otp number:".$rndno);
				$route="route=4";
				$authkey="213315AtaiOwL5FF5ae8a946";
				$postData=array(
				'authkey'=>$authkey,
				'mobiles'=>$mobile,
				'message'=>$message,
				'sender'=>$senderId,
				'route'=>$route);
				$url="https://control.msg91.com/api/sendhttp.php";

				$ch=curl_init();
				curl_setopt_array($ch,array(
				CURLOPT_URL=>$url,
				CURLOPT_RETURNTRANSFER=>true,
				CURLOPT_POST=>true,
				CURLOPT_POSTFIELDS=>$postData
				));
				//curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,0);
				//curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,0);
				$output=curl_exec($ch);
				if(curl_errno($ch)){
					echo 'error:'.curl_error($ch);
				}
				curl_close($ch); 
		}
		catch(Exception $e){
		    echo $e->getMessage();
		}
			
			 $myArray = ['code'=>200,'otp'=>$rndno,'user_id'=>$user_id ];
                  return response()->json($myArray);
				  
				  
		    
   }
   public function forgot_password(Request $request){
	       $password = $request->post('password');
		  $user_id = $request->post('user_id');
	      $passwordd=bcrypt($password);
 
 
 
          DB::table('users')->where('user_id',$user_id)->update(['password'=>$passwordd]);
			  
			  $myArray = ['code'=>200, 'msg'=>'Password Reset Successfully'];
                  return response()->json($myArray);
		
	 
   }
   public function search_user_data(Request $request){
        $search_input = $request->post('search_input');
		
		if(!($search_input)){
		  $myArray = ['code'=>500, 'msg'=>'Plz Type in Search Box '];
                  return response()->json($myArray);
		}
		else
		{
		   $user = DB::table('users')->orwhere('user_id',$search_input)->orwhere('email',$search_input)->select('*')->get();
		   if(count($user)== 0){
			    $myArray = ['code'=>201, 'msg'=>'Could not match your search criteria.'];
                  return response()->json($myArray);
			   
		   }
		   else{
			   $role_id = $user[0]->role_id; 
			   $user_id = $user[0]->user_id;

			    if($role_id == '1'){
			       $expe=DB::table('emp_personal_details')->select('*')->where('user_id',$user_id)->get();
			       $position=$expe[0]->position;
					if($position=='Web Designer'){
					$myArray = ['code'=>205, 'user_id'=>$user_id];
                        return response()->json($myArray);
					}
					elseif($position=='Web Developer'){
						$myArray = ['code'=>206, 'user_id'=>$user_id];
                        return response()->json($myArray);
					}
					else{
						$myArray = ['code'=>207, 'user_id'=>$user_id];
                        return response()->json($myArray);
					}
					
				}
				else{
					$myArray = ['code'=>200, 'user_id'=>$user_id];
                        return response()->json($myArray);
				}
			
   			 
		   }
		   
		        
				
		}  
		
 
   }
   
    public function getcomprofile($id){ 
	
	
					  $users=DB::table('users')->select('*')->where('user_id',$id)->get();
					   $companyinfo=DB::table('com_infos')->select('*')->where('company_user_id',$id)->get();
					   $directors=DB::table('com_director_details')->select('*')->where('company_user_id',$id)->get();
					   $business=DB::table('com_business_details')->select('*')->where('company_user_id',$id)->get();
					   $newjobs=DB::table('company_jobs')->select('*')->where('company_id',$id)->get();
		               $newprojects=DB::table('company_projects')->select('*')->where('company_id',$id)->get();
					  
					  
					   $myArray = ['newjobs'=>$newjobs,'newprojects'=>$newprojects,'users'=>$users, 'companyinfo'=>$companyinfo,'directors'=>$directors,'business'=>$business];
					   
								  return response()->json($myArray); 
					
	}

	 public function get_des_emp_profile($id){ 
	
	
					   $users=DB::table('users')->select('*')->where('user_id',$id)->get();
					   $edu=DB::table('emp_educations')->select('*')->where('user_id',$id)->get();
					   $personal=DB::table('emp_personal_details')->select('*')->where('user_id',$id)->get();
					   $expe=DB::table('emp_experiences')->select('*')->where('user_id',$id)->get();
					   $skill=DB::table('emp_skills')->select('*')->where('user_id',$id)->get();
                       $approve='1';					    
					   $projects= DB::table('project-assign-user')
                                  ->join('company_projects','company_projects.id','project-assign-user.project_id')	
								  ->join('users','users.user_id','project-assign-user.user_id')	
                                  ->where(['project-assign-user.user_id'=>$id,'users.approve_user'=>$approve])
								  ->select('*')								  
					              ->get(); 
					  
					  
					 
			
					  $myArray = ['users'=>$users, 'education'=>$edu,'personal'=>$personal,'expe'=>$expe,'skill'=>$skill,'projects'=>$projects];
								  return response()->json($myArray);
					 
					
	} 
 public function get_dev_emp_profile($id){ 	
	                  $users=DB::table('users')->select('*')->where('user_id',$id)->get();
					   $edu=DB::table('emp_educations')->select('*')->where('user_id',$id)->get();
					   $personal=DB::table('emp_personal_details')->select('*')->where('user_id',$id)->get();
					   $expe=DB::table('emp_experiences')->select('*')->where('user_id',$id)->get();
					   $skill=DB::table('emp_skills')->select('*')->where('user_id',$id)->get();
					   $approve='1';
					   $projects= DB::table('project-assign-user')
                                  ->join('company_projects','company_projects.id','project-assign-user.project_id')	
								  ->join('users','users.user_id','project-assign-user.user_id')	
                                  ->where(['project-assign-user.user_id'=>$id,'users.approve_user'=>$approve])
								  ->select('*')								  
					              ->get(); 
					  
					  
					   $myArray = ['users'=>$users, 'education'=>$edu,'personal'=>$personal,'expe'=>$expe,'skill'=>$skill,'projects'=>$projects];
								  return response()->json($myArray);
 }
}
