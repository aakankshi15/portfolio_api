<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


use App\User;
use DB;
use Session;
use Cookie;
use Auth;
use Response;

class EditCompanyProfileController extends Controller
{
    
	public function upload_company_back_image(Request $request){
		 
		   $user_id= $request->post('user_id');
		   
      if($request->file('profile_image')){		 
		 $image = $request->file('profile_image');
	   	 $name = time() . '.' . $image->getClientOriginalExtension();
		 $path = public_path('images');
		
		  if ( $image->move( $path, $name ) != false || ! is_null( $image->move( $path, $name))) {
			$file = "File Uploaded Successfully";
		   }
		   else {
		 	$file = "Error";
		    }
		  
		   DB::table('com_infos')->where('company_user_id',$user_id)->update(['company_logo'=>$name]);  
		 
		  $myArray = ['code'=>200, 'msg'=>'Successfully Update'];
                  return response()->json($myArray);
	  }
	  else 
	  {
		 $image = $request->file('background_image');
	   	 $name = time() . '.' . $image->getClientOriginalExtension();
		 $path = public_path('images');
		
		if ( $image->move( $path, $name ) != false || ! is_null( $image->move( $path, $name))) {
			$file = "File Uploaded Successfully";
		 }
		 else {
		 	$file = "Error";
		 }
		  
		 DB::table('com_infos')->where('company_user_id',$user_id)->update(['back_image'=>$name]);  
		 
		 $myArray = ['code'=>200, 'msg'=>'Successfully Update'];
                  return response()->json($myArray);
	  }
}


        public function delete_jobs($id){
			DB::table('company_jobs')->where('id',$id)->delete();
			 $myArray = ['msg'=>'successfully deleted'];
                  return response()->json($myArray);
		}
		public function get_jobs($id)
        {
			
			  	$job=DB::table('company_jobs')->select('*')->where('id',$id)->get();
			
			    $myArray = ['job'=>$job];
                  return response()->json($myArray);
		}
		public function edit_job(Request $request)
		{                       
		    
              $id     =  $request->input('id');
   			  $category     =  $request->input('category');
			  $designation     =  $request->input('designation');
			  $description     =  $request->input('description');
			  $eligibility     =  $request->input('eligibility');
			  $location     =  $request->input('location');
			  $skill_required     =  $request->input('skill_required');
			  $salary     =  $request->input('salary');
			  $experience     =  $request->input('experience');
			  
			  DB::table('company_jobs')->where('id',$id)->update(['category'=>$category,'designation'=>$designation,'description'=>$description,'eligibility'=>$eligibility,'location'=>$location,'skill_required'=>$skill_required,'salary'=>$salary,'experience'=>$experience]);
			   
			   $myArray = [ 'code'=>200,'msg'=>'Successfully Update'];
                  return response()->json($myArray);
		
		}
		
		 public function delete_projects($id){
			DB::table('company_projects')->where('id',$id)->delete();
			 $myArray = ['msg'=>'successfully deleted'];
                  return response()->json($myArray);
		}
		public function get_projects($id)
        {
			
			  	$job=DB::table('company_projects')->select('*')->where('id',$id)->get();
			
			    $myArray = ['job'=>$job];
                  return response()->json($myArray);
		}
		public function edit_projects(Request $request)
		{                       
		      
             $id     =  $request->input('id');
			 $arr = $request->input('arr');
		     $user_status ='1';
			 if(count($arr)!= 0){
    		   foreach($arr as $arraydata ){
				  $user_id = $arraydata;  
				  DB::table('project-assign-user')->insert(['user_id'=>$user_id,'project_id'=>$id,'user_status'=>$user_status]); 
			   } 
			 }
			
   			  $project_name     =  $request->input('project_name');
			  $project_description     =  $request->input('project_description');
		
			 $image = $request->file('project_images');
			  if(empty($image)){
				 
				     DB::table('company_projects')->where('id',$id)->update(['project_name'=>$project_name,'project_description'=>$project_description]);
			  }
			  else{
				   
	      	      $name = time() . '.' . $image->getClientOriginalExtension();
		          $path = public_path('images');
		
		          if ($image->move( $path, $name ) != false || ! is_null( $image->move( $path, $name))) {
		     	  $file = "File Uploaded Successfully";
		          }
		          else {
	      	 	   $file = "Error";
		          }
			  
			     DB::table('company_projects')->where('id',$id)->update(['project_name'=>$project_name,'project_description'=>$project_description,'project_images'=>$name]);
			
			  }
			   $myArray = [ 'code'=>200,'msg'=>'Successfully Update'];
                  return response()->json($myArray);
		
		}
		
		  public function selected_user($project_id)
	     {
		  
		  $empData=DB::table('project-assign-user')
		  ->join('emp_personal_details','emp_personal_details.user_id','project-assign-user.user_id')
		  ->select(['emp_personal_details.employee_name','project-assign-user.id'])
		  ->where('project-assign-user.project_id',$project_id)->get();
		
	       $myArray = ['code'=>200,'empData'=> $empData];
                  return response()->json($myArray);
	    }
		 public function delete_assign_projects($id){
			DB::table('project-assign-user')->where('id',$id)->delete();
			 $myArray = ['msg'=>'successfully deleted'];
                  return response()->json($myArray);
		}
			
}
