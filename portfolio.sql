-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 16, 2018 at 12:05 PM
-- Server version: 5.7.24-0ubuntu0.16.04.1
-- PHP Version: 7.1.23-2+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `portfolio`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_slide_show`
--

CREATE TABLE `admin_slide_show` (
  `id` int(10) UNSIGNED NOT NULL,
  `slider_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_slide_show`
--

INSERT INTO `admin_slide_show` (`id`, `slider_image`, `title`, `created_at`, `updated_at`) VALUES
(3, '1539170275.jpg', 'first', '2018-10-10 11:17:55', '2018-11-15 13:21:23'),
(4, '1539170315.jpg', 'second', '2018-10-10 11:18:35', '2018-10-10 11:18:35'),
(5, '1539170350.jpg', 'third', '2018-10-10 11:19:10', '2018-10-10 11:19:10');

-- --------------------------------------------------------

--
-- Table structure for table `admin_testimonials`
--

CREATE TABLE `admin_testimonials` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_testimonials`
--

INSERT INTO `admin_testimonials` (`id`, `name`, `description`, `image`, `created_at`, `updated_at`) VALUES
(4, 'Jerry pal', 'orem ipsum dolor sit amet adipiscing elit am nibh unc varius facilisis eros ed erat in in velit quis arcu ornare laoreet urabitur sit amet adipiscing elit am nibh unc varius facilisis eros ed erat in in velit quis arcu ornare laoreet urabitur Ben Hanna', '1539669494.jpg', '2018-10-16 05:58:14', '2018-10-16 05:59:40'),
(5, 'Ben Hanna', 'Lorem ipsum dolor sit amet adipiscing elit am nibh unc varius facilisis eros ed erat in in velit quis arcu ornare laoreet urabitur sit amet adipiscing elit am nibh unc varius facilisis eros ed erat in in velit quis arcu ornare laoreet urabitur', '1539669605.jpg', '2018-10-16 06:00:05', '2018-10-16 06:00:05'),
(6, 'Aakankshi Gupta', 'orem ipsum dolor sit amet adipiscing elit am nibh unc varius facilisis eros ed erat in in velit quis arcu ornare laoreet urabitur sit amet adipiscing elit am nibh unc varius facilisis eros ed erat in in velit quis arcu ornare laoreet urabitur Ben Hanna', '1539670392.jpeg', '2018-10-16 06:13:12', '2018-10-16 06:13:12');

-- --------------------------------------------------------

--
-- Table structure for table `admin_top_ranks`
--

CREATE TABLE `admin_top_ranks` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

CREATE TABLE `blogs` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `author_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_date` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blogs`
--

INSERT INTO `blogs` (`id`, `user_id`, `post_title`, `post_description`, `author_name`, `post_date`, `post_image`, `created_at`, `updated_at`) VALUES
(1, '1538652055873', 'jkjk', 'dcvefvfv', 'fvfvfv', '5675867', '1538986286.jpg', '2018-10-08 08:11:26', '2018-10-08 08:11:26'),
(6, '1538650932588', 'c', 'cumint private limited', '15/08/1997', '2018/10/10', '1539151943.png', '2018-10-10 06:12:23', '2018-10-10 06:12:23'),
(7, '1538650932588', 'Aakankshi Gupta', 'fdgghfgh', 'ghfghfgh', '2018/10/10', '1539152203.png', '2018-10-10 06:16:43', '2018-10-10 06:16:43');

-- --------------------------------------------------------

--
-- Table structure for table `company_jobs`
--

CREATE TABLE `company_jobs` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `designation` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `eligibility` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `skill_required` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `salary` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `experience` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `company_jobs`
--

INSERT INTO `company_jobs` (`id`, `company_id`, `category`, `designation`, `description`, `eligibility`, `location`, `skill_required`, `salary`, `experience`, `created_at`, `updated_at`) VALUES
(4, '1538650932588', 'Hardware', 'afdsdafd', 'Sandeep', '9/20/2018', 'sandeep@cuminte.com', '7753059779', 'Indrapuram ghaziabad', '1', '2018-10-10 06:19:34', '2018-11-16 07:03:39'),
(5, '1538650932588', 'Hardware', 'DATA ENTRY H', 'nothing', 'mca', 'noida', 'nothing', '546', '2 years', '2018-11-16 06:55:43', '2018-11-16 07:04:00');

-- --------------------------------------------------------

--
-- Table structure for table `company_projects`
--

CREATE TABLE `company_projects` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `project_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `project_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `project_images` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `company_projects`
--

INSERT INTO `company_projects` (`id`, `company_id`, `project_name`, `project_description`, `project_images`, `created_at`, `updated_at`) VALUES
(3, '1538652055873', 'java', '3453-12-31T18:30:00.000Z', '1538742748.png', '2018-10-05 12:32:28', '2018-10-05 12:32:28'),
(4, '1538650932588', 'logo t datah', 'nothingJ', '1542354187.jpg', '2018-10-08 06:09:04', '2018-11-16 07:50:23'),
(5, '1538650932588', 'testing', 'testing purpose', '1542260582.jpg', '2018-11-15 05:38:07', '2018-11-15 05:43:02');

-- --------------------------------------------------------

--
-- Table structure for table `com_business_details`
--

CREATE TABLE `com_business_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_user_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `business_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `business_detail` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `com_business_details`
--

INSERT INTO `com_business_details` (`id`, `company_user_id`, `business_type`, `business_detail`, `created_at`, `updated_at`) VALUES
(1, '1538650932588', 'Technology', 'php ', '2018-10-04 11:19:45', '2018-10-04 11:19:45'),
(2, '1538652055873', 'Technology', 'Salesforce Development', '2018-10-04 11:25:16', '2018-10-04 11:25:16'),
(3, '1542356406817', 'Consulting', '7uyjhjh', '2018-11-16 08:21:09', '2018-11-16 08:21:09');

-- --------------------------------------------------------

--
-- Table structure for table `com_director_details`
--

CREATE TABLE `com_director_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_user_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `com_director_details`
--

INSERT INTO `com_director_details` (`id`, `company_user_id`, `first_name`, `last_name`, `email`, `contact`, `address`, `created_at`, `updated_at`) VALUES
(1, '1538650932588', 'Sandeep', 'Mishra', 'sadepp@gmail.com', '7567767765', 'Indrapuram Ghaziabad', '2018-10-04 11:16:56', '2018-10-04 11:16:56'),
(2, '1538650932588', 'Bharat ', 'Khanna', 'bharat@gmail.com', '77530529779', 'Basant Vihar New Dehli', '2018-10-04 11:16:56', '2018-11-16 06:48:57'),
(3, '1538652055873', 'Ajay ', 'Tiwari', 'ajay@gmail.com', '76887867987', 'noida sector 62', '2018-10-04 11:24:28', '2018-10-04 11:24:28'),
(4, '1542356406817', 'Demo', 'Team', 'aakankshi@gmail.com', '8745878785', 'Berlin, Germany', '2018-11-16 08:21:04', '2018-11-16 08:21:04');

-- --------------------------------------------------------

--
-- Table structure for table `com_infos`
--

CREATE TABLE `com_infos` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_user_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_logo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `back_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `postal_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `com_infos`
--

INSERT INTO `com_infos` (`id`, `company_user_id`, `company_name`, `company_id`, `company_url`, `company_logo`, `back_image`, `country`, `state`, `city`, `postal_code`, `created_at`, `updated_at`) VALUES
(1, '1538650932588', 'Cuminte Private Limited', 'Cum11224', 'www.cuminte.com', '1539153354.jpg', NULL, 'Other', 'Uttar Pradesh', 'Noida', '201305', '2018-10-04 11:15:32', '2018-11-16 06:48:35'),
(2, '1538652055873', 'Cloud Analogy', 'Cloud56565', 'www.cloud.com', '1538652188.jpg', NULL, 'Other', 'Uttar Pradesh', 'Noida', '201301', '2018-10-04 11:23:08', '2018-10-04 11:23:08'),
(3, '1542356406817', 'dfgvfv', 'dhfgh6678', 'www.cuminte.com', '1542356452.jpg', NULL, 'Australia', 'Punjab', 'Noida', '67867867', '2018-11-16 08:20:52', '2018-11-16 08:20:52');

-- --------------------------------------------------------

--
-- Table structure for table `emp_educations`
--

CREATE TABLE `emp_educations` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `course_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `institute_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `year_of_passing` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `course_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `emp_educations`
--

INSERT INTO `emp_educations` (`id`, `user_id`, `course_id`, `institute_name`, `year_of_passing`, `course_type`, `created_at`, `updated_at`) VALUES
(1, '1538652482548', 'Graduation', 'SDDIC RUDAIN', '2011-03-09T18:30:00.000Z', 'Part Time', '2018-10-04 11:32:39', '2018-10-10 06:51:20'),
(2, '1538652482548', 'Intermediate', 'SAKPIC Rudain', '2013-02-04T18:30:00.000Z', 'Correspondence', '2018-10-04 11:32:39', '2018-10-10 06:51:36'),
(3, '1538652947574', 'Highschool', 'sddic rudain', '2018-06-11T18:30:00.000Z', 'Full Time', '2018-10-04 11:37:20', '2018-10-04 11:37:20'),
(4, '1538652947574', 'Graduation', 'BBIDIT Islamnagar', '2018-10-08T18:30:00.000Z', 'Full Time', '2018-10-04 11:37:20', '2018-10-04 11:37:20'),
(5, '1538653239958', '9536063146', 'Bharat ', 'Khanna', 'bharat@cuminte.com', '2018-10-04 11:43:38', '2018-10-04 11:43:38');

-- --------------------------------------------------------

--
-- Table structure for table `emp_experiences`
--

CREATE TABLE `emp_experiences` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_Name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `employee_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `time_prefered` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `work_exp_year` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `work_exp_month` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_of_joining` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_of_closing` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `emp_experiences`
--

INSERT INTO `emp_experiences` (`id`, `user_id`, `company_Name`, `employee_id`, `position`, `time_prefered`, `work_exp_year`, `work_exp_month`, `date_of_joining`, `date_of_closing`, `created_at`, `updated_at`) VALUES
(3, '1538652482548', 'Cloud Analogy', 'Cum123356', 'Web Developer', 'Day Shift', '1 Year', '0 Months', '2018-01-30T18:30:00.000Z', '2018-10-10T18:30:00.000Z', '2018-10-04 11:35:14', '2018-10-10 07:08:26'),
(4, '1538652947574', 'Cuminte Private Limited', 'Cum12335', 'Web Developer', 'Day Shift', '1 Year', '0 Months', '2018-10-14T18:30:00.000Z', '2018-10-13T18:30:00.000Z', '2018-10-04 11:38:09', '2018-10-04 11:38:09');

-- --------------------------------------------------------

--
-- Table structure for table `emp_personal_details`
--

CREATE TABLE `emp_personal_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `employee_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `current_company` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `position` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_of_joining` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `employee_dob` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `employee_pincode` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_num` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `employee_address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `employee_gender` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `employee_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `back_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `employee_resume` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `emp_personal_details`
--

INSERT INTO `emp_personal_details` (`id`, `user_id`, `employee_name`, `current_company`, `company_id`, `position`, `date_of_joining`, `employee_dob`, `employee_pincode`, `contact_num`, `employee_address`, `employee_gender`, `employee_image`, `back_image`, `employee_resume`, `created_at`, `updated_at`) VALUES
(1, '1538652482548', 'Akankshi Gupta', 'Cuminte Private Limited', '1538650932588', 'Web Developer', '10/23/2018', '10/23/2018', '201203', '7753059779', 'Nest Home Industrial Area Sector 62', 'female', '1542344925.jpg', '1539175422.jpg', NULL, '2018-10-04 11:31:26', '2018-11-16 05:08:45'),
(2, '1538652947574', 'Zuvair Hasan', 'Cloud Analogy', '1538652055873', 'Web Designer', '10/23/2018', '10/23/2018', '656565', '54465465455', 'Noida 62', 'male', '1538653009.jpg', NULL, NULL, '2018-10-04 11:36:49', '2018-10-11 11:43:55'),
(3, '1538653239958', 'Hari Om Singh', 'Cuminte Private Limited', '1538650932588', 'Web Designer', '10/23/2018', '10/23/2018', '201301', '775305766', 'Mamura Chok', 'male', '1538655427.jpg', NULL, NULL, '2018-10-04 11:42:16', '2018-10-11 11:44:08'),
(4, '1538653843873', 'Ramu Signh', 'Cloud Analogy', '1538652055873', 'Web Designer', '10/23/2018', '10/23/2018', '775305', '56547675667', 'noida ', 'male', NULL, NULL, NULL, '2018-10-04 11:53:02', '2018-10-11 11:44:00'),
(5, '1538660605341', 'Ritu Priya', 'Cuminte Private Limited', '1538650932588', 'Web Developer', '10/23/2018', '10/23/2018', '364575', '9711280827', 'Delhi ', 'female', NULL, NULL, NULL, '2018-10-04 13:44:20', '2018-10-11 11:44:10'),
(10, '1542355483161', 'JKLJKLJKL', 'Cuminte Private Limited', '1538650932588', 'Web Designer', '2018-09-19T18:30:00.000Z', '2018-11-14T18:30:00.000Z', '67868', 'JKJKLJKL', '67867868', 'female', NULL, NULL, NULL, '2018-11-16 08:05:03', '2018-11-16 08:05:03'),
(11, '1542368507733', 'Tester', 'Cloud Analogy', '1538652055873', 'Web Designer', '2018-11-11T18:30:00.000Z', '2018-11-14T18:30:00.000Z', '234354', '7753059779', 'Nehru Place, New Delhi, Delhi, India', 'female', NULL, NULL, NULL, '2018-11-16 11:42:31', '2018-11-16 11:42:31'),
(12, '1542368571351', 'tester2', 'Cloud Analogy', '1538652055873', 'Web Developer', '2018-11-04T18:30:00.000Z', '2018-11-08T18:30:00.000Z', '234354', '7753059779', 'Nehru Place, New Delhi, Delhi, India', 'male', NULL, NULL, NULL, '2018-11-16 11:43:41', '2018-11-16 11:43:41');

-- --------------------------------------------------------

--
-- Table structure for table `emp_skills`
--

CREATE TABLE `emp_skills` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `employee_skill` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `experience` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `emp_skills`
--

INSERT INTO `emp_skills` (`id`, `user_id`, `employee_skill`, `experience`, `created_at`, `updated_at`) VALUES
(1, '1538652482548', 'Angular 6', '2 Year', '2018-10-04 11:33:07', '2018-10-10 07:00:18'),
(2, '1538652482548', 'Laravel 5.5', '3 Year', '2018-10-04 11:33:07', '2018-10-10 07:00:18'),
(3, '1538652947574', 'Android Developer', '1 Year', '2018-10-04 11:37:46', '2018-10-04 11:37:46'),
(4, '1538652947574', 'Magento Expert', '1 Year', '2018-10-04 11:37:46', '2018-10-04 11:37:46');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(4, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(5, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(6, '2016_06_01_000004_create_oauth_clients_table', 1),
(7, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(8, '2018_09_10_101936_emp_personal_detail', 1),
(9, '2018_09_10_102021_emp_education', 1),
(10, '2018_09_10_102039_emp_skill', 1),
(11, '2018_09_10_102048_emp_experience', 1),
(12, '2018_09_10_102130_com_information', 1),
(13, '2018_09_10_102142_com_director_detail', 1),
(14, '2018_09_10_102214_com_business_detail', 1),
(15, '2018_09_13_104254_add_blog', 1),
(16, '2018_09_14_105613_add_jobs', 1),
(17, '2018_09_14_123820_add-projects', 1),
(18, '2018_09_25_054305_slide-show', 1),
(19, '2018_09_25_054358_testimonials', 1),
(20, '2018_09_25_054837_top-ranks', 1),
(21, '2018_10_08_063615_project-assign-user', 2);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('020addbbab1f2991ebcb78d8049f45351e2342766331cbe7b34b57a9b1322b3d4faa75c971ceebab', 2, 1, 'MyApp', '[]', 0, '2018-10-09 12:32:10', '2018-10-09 12:32:10', '2019-10-09 12:32:10'),
('0299d48bf306e7a434967f47d9cf27135f0713fbbfe886bb28d5db1f6fac669af2b033c9125fd9af', 5, 1, 'MyApp', '[]', 0, '2018-10-04 12:42:21', '2018-10-04 12:42:21', '2019-10-04 12:42:21'),
('02d7ce3152ec6523c4bc07bcda2bde9a4d4d80cae2e8431faa929d312d515415635dba5f1822bb89', 13, 1, 'MyApp', '[]', 0, '2018-11-15 10:59:58', '2018-11-15 10:59:58', '2019-11-15 10:59:58'),
('03e993de0f84f09159a981c9b15210fd5c13ecf3b2ea0cb4ee7cb36a92bec2a64adc52621d77c621', 2, 1, 'MyApp', '[]', 0, '2018-10-11 11:31:13', '2018-10-11 11:31:13', '2019-10-11 11:31:13'),
('04014d9b138b0adbcbc5f9e69b3db2aed553cb84ddf33f167370e8fd54019dae9e333013c6dfa8c0', 2, 1, 'MyApp', '[]', 0, '2018-10-04 12:43:18', '2018-10-04 12:43:18', '2019-10-04 12:43:18'),
('049dbd2a1d4536cd0d54fce235721e10dace7d9b00c6674e1f097a7fbf83b4e691717c29cfff0d01', 2, 1, 'MyApp', '[]', 0, '2018-10-05 07:52:18', '2018-10-05 07:52:18', '2019-10-05 07:52:18'),
('05404b293b4f79dda924ac1140afd321910adef9c43546c160300d5939d56be9036a2432742092e5', 2, 1, 'MyApp', '[]', 0, '2018-11-15 05:19:18', '2018-11-15 05:19:18', '2019-11-15 05:19:18'),
('0867797c44c315175f3a631a57a84c0bb4704f9c5b891ea563895c1ae715f06a7f803fe889ad8e01', 2, 1, 'MyApp', '[]', 0, '2018-10-09 04:51:52', '2018-10-09 04:51:52', '2019-10-09 04:51:52'),
('09ae3924d94950c09bba3660ce7620dc4cd03378d823e92f8ae54b12ee982910254c69b917f6b876', 6, 1, 'MyApp', '[]', 0, '2018-10-04 12:10:05', '2018-10-04 12:10:05', '2019-10-04 12:10:05'),
('0bf3eb00ce293073131df073a2842a6339f053aa900a8e753177327902a3599c190b1830598c1320', 5, 1, 'MyApp', '[]', 0, '2018-10-15 07:43:57', '2018-10-15 07:43:57', '2019-10-15 07:43:57'),
('0e2cbc4a546af1292953b43cbd29b9fc4d9b236d72ac3a9eeff2f09afc710e726bf5e2be653778f8', 13, 1, 'MyApp', '[]', 0, '2018-11-15 11:49:06', '2018-11-15 11:49:06', '2019-11-15 11:49:06'),
('123559a868d71c452aa5fc80333f0b1cfa6db83175b665ba9647f3e28bdc2789f8703bcb91b86a43', 4, 1, 'MyApp', '[]', 0, '2018-10-11 11:07:07', '2018-10-11 11:07:07', '2019-10-11 11:07:07'),
('130473014ad2c385770625a73134b9696c2655b7715d381bc90bb76797c8e95b1e497f73c148e980', 13, 1, 'MyApp', '[]', 0, '2018-11-15 11:25:18', '2018-11-15 11:25:18', '2019-11-15 11:25:18'),
('1400245ffca2e1853c087a435737c45edb3f73f758d9c7af0a0cbf99433e4c7aa56d1035b6c0aed0', 4, 1, 'MyApp', '[]', 0, '2018-11-15 05:36:19', '2018-11-15 05:36:19', '2019-11-15 05:36:19'),
('1464e7928a49f0afb1bd9dc7f2f7acaaa2cffc59b5e25f7cdf82e430053b3fd1862ad34f6356349d', 3, 1, 'MyApp', '[]', 0, '2018-11-16 11:40:10', '2018-11-16 11:40:10', '2019-11-16 11:40:10'),
('147fff30709a66a8c1cb4e93c0167628795907e9d516592a4f23dca6ebb05e97da7115711facc315', 4, 1, 'MyApp', '[]', 0, '2018-10-04 12:22:12', '2018-10-04 12:22:12', '2019-10-04 12:22:12'),
('15316ef80d48501c9261f0f658803c758cb8e5c8537f04f4d04f9bf557facc0947983f97f8c3a40c', 7, 1, 'MyApp', '[]', 0, '2018-10-04 11:53:39', '2018-10-04 11:53:39', '2019-10-04 11:53:39'),
('172574cc8b4e8b8335cc6149b1a21953197db754b236c67882c31eec7413a4fd111e67dc16f35484', 13, 1, 'MyApp', '[]', 0, '2018-11-15 10:26:05', '2018-11-15 10:26:05', '2019-11-15 10:26:05'),
('18e397e822895d4079d39849b0592516de6ec0ecd33f8d6cdec3563a45b74eef0ba11af72340a1a6', 3, 1, 'MyApp', '[]', 0, '2018-10-04 11:57:12', '2018-10-04 11:57:12', '2019-10-04 11:57:12'),
('1e0d240c510d10786fe95940e2e7448ab3f299b51d1447a9f12558a651cf1604439f396a0cabb96c', 13, 1, 'MyApp', '[]', 0, '2018-11-15 11:08:22', '2018-11-15 11:08:22', '2019-11-15 11:08:22'),
('23912522d0fa71454645ed2833f82020ef917ac8fa774411493833717bb02a478c43109be25a39ef', 5, 1, 'MyApp', '[]', 0, '2018-10-11 11:37:14', '2018-10-11 11:37:14', '2019-10-11 11:37:14'),
('256503923e8ef509364d505a292dded125874f3c9992053b456f15f45f8d682abb989e78c32d1a4f', 4, 1, 'MyApp', '[]', 0, '2018-10-09 05:28:46', '2018-10-09 05:28:46', '2019-10-09 05:28:46'),
('26b931440d8327711a7cb034ec22cdf49e4475a8039f64413514efa2b4ea28a09eba1a7c56452831', 4, 1, 'MyApp', '[]', 0, '2018-11-16 11:20:14', '2018-11-16 11:20:14', '2019-11-16 11:20:14'),
('287cf62bb5206f1a3bf68b40d59ae24e23ffc14d72a874aaf8d27605b2d6962d56d170b3bd8f150e', 2, 1, 'MyApp', '[]', 0, '2018-10-11 10:45:35', '2018-10-11 10:45:35', '2019-10-11 10:45:35'),
('291a217e09f84cb7de2170e848861a6db9420938ad117854f7fa86c02258b681e413ede189fb8f98', 4, 1, 'MyApp', '[]', 0, '2018-10-10 06:25:36', '2018-10-10 06:25:36', '2019-10-10 06:25:36'),
('2923cf83f5d9e2f6cf21b1d594a45597829ab601155f91ee8aec697f1c0c502e9526179f94985910', 3, 1, 'MyApp', '[]', 0, '2018-10-08 08:09:32', '2018-10-08 08:09:32', '2019-10-08 08:09:32'),
('2d6fe377ef6b9c8e9ef845e9c73fd6a67b2dcb3739fd63b1a46b55eb398f2731050c529ccbb003d4', 3, 1, 'MyApp', '[]', 0, '2018-10-05 07:50:21', '2018-10-05 07:50:21', '2019-10-05 07:50:21'),
('2e0d4175084eff0b0b9fd95c68a2e0831bf52831d5f07dd37dd2813975a91bcca181e78eeecff6d1', 6, 1, 'MyApp', '[]', 0, '2018-10-04 12:04:03', '2018-10-04 12:04:03', '2019-10-04 12:04:03'),
('2f8c6752891b0b1175ddedf8721426be9b2692f4816b150bece7d4b8ae87fb8913938d76ef470387', 4, 1, 'MyApp', '[]', 0, '2018-10-10 07:29:38', '2018-10-10 07:29:38', '2019-10-10 07:29:38'),
('2fe7263f3cd52856b962b8a1818bbde8e20a062f77192c16a879a031187b273354c0d197c87b8cb9', 3, 1, 'MyApp', '[]', 0, '2018-10-04 12:43:29', '2018-10-04 12:43:29', '2019-10-04 12:43:29'),
('301b30c81f906674c569d1f717bd3f29cfdc631e7e44feda21560793d092db0e9c13289e3db35d94', 5, 1, 'MyApp', '[]', 0, '2018-10-04 11:50:07', '2018-10-04 11:50:07', '2019-10-04 11:50:07'),
('30fb34afa9b6fa2943c0b7a944a2c03011d95dbd82dbca192ab747d0658262dd248e46d979bf4d46', 4, 1, 'MyApp', '[]', 0, '2018-10-05 04:57:22', '2018-10-05 04:57:22', '2019-10-05 04:57:22'),
('364688dfcef9531e612221105efee79fa52d1bb06ceb7d4a4dabf2cef22f57a835f6ecd023207d77', 4, 1, 'MyApp', '[]', 0, '2018-10-15 07:28:37', '2018-10-15 07:28:37', '2019-10-15 07:28:37'),
('370c19cbc2425ec5cab33babd12e0e99d8884160541aa5d87b47c51981a0a7de296bc51cb0177375', 4, 1, 'MyApp', '[]', 0, '2018-11-15 05:37:05', '2018-11-15 05:37:05', '2019-11-15 05:37:05'),
('371651731b787cfef8342ac77636606afc2f8c2fd8e839b5e953dda5127be14e85e0b487637eb21f', 2, 1, 'MyApp', '[]', 0, '2018-10-09 10:27:15', '2018-10-09 10:27:15', '2019-10-09 10:27:15'),
('3878e06ddf041a4bc0339ca58ad66c5c7ebac1917b87f9eed49c9c1b5919c4404ca37da44ab5ef95', 2, 1, 'MyApp', '[]', 0, '2018-10-12 11:50:03', '2018-10-12 11:50:03', '2019-10-12 11:50:03'),
('3bfc96adc0155f90d5e17d1aa2c25cc68817994c322f6ba26d2cb8a7d7e3115f4baff4b1dc119e4f', 4, 1, 'MyApp', '[]', 0, '2018-10-05 05:38:58', '2018-10-05 05:38:58', '2019-10-05 05:38:58'),
('40370fdfd5b32651a9c694196cd2eb2d090557a8087cd3a08feba14ddb4b104c94fb4475308aad7a', 4, 1, 'MyApp', '[]', 0, '2018-11-15 05:18:49', '2018-11-15 05:18:49', '2019-11-15 05:18:49'),
('41412e0a2a1ff1e26c37b85fa4c5119c73458f9843c1a062513996dfac25c5fa306520ae220755bb', 6, 1, 'MyApp', '[]', 0, '2018-10-04 11:43:01', '2018-10-04 11:43:01', '2019-10-04 11:43:01'),
('414a006fab0a38b7c0f6d322e6acfbb48492e25876d1cacf36676b55596183f14d073e81dbe579f7', 6, 1, 'MyApp', '[]', 0, '2018-10-04 12:19:11', '2018-10-04 12:19:11', '2019-10-04 12:19:11'),
('4259f104465f767a2586f31c55e1d19fa7778d791ff337d0bfac0cc95a6a07a6ea51179948e47643', 5, 1, 'MyApp', '[]', 0, '2018-10-05 07:49:46', '2018-10-05 07:49:46', '2019-10-05 07:49:46'),
('43f61e6bd97c332367223c1db462a12182c78d56b9f5edaa3c97201f50b08901ee40ca373f1feae8', 4, 1, 'MyApp', '[]', 0, '2018-10-04 11:38:50', '2018-10-04 11:38:50', '2019-10-04 11:38:50'),
('453a605b3a2a76fb64e6f4db4dc05049c9e77d1ef2f0c661be24697ab19ccee83e28fe3e7d5f0922', 2, 1, 'MyApp', '[]', 0, '2018-10-10 07:55:14', '2018-10-10 07:55:14', '2019-10-10 07:55:14'),
('454a372f2ca86ae18a32f47e28687c0fcaa2f7ad2b320a6f4f39d15fd988dd03b7b874b909e55fb2', 3, 1, 'MyApp', '[]', 0, '2018-10-08 08:08:38', '2018-10-08 08:08:38', '2019-10-08 08:08:38'),
('4632c0fbe60c5eaf3ddde6392ed558ed8bb2106c49b8d12ce854a6814b7630ee6f35ba4c6118bc5d', 2, 1, 'MyApp', '[]', 0, '2018-10-08 12:07:35', '2018-10-08 12:07:35', '2019-10-08 12:07:35'),
('4685503328a849fca563ac827a0726bc9996e130c6576d9861b59dd5232df542951ccbe4591f3728', 13, 1, 'MyApp', '[]', 0, '2018-11-15 11:38:11', '2018-11-15 11:38:11', '2019-11-15 11:38:11'),
('487fe45ff7c12c9d3a69b6c038da27e0ead9073c9927daf1313c50d37da80f190c8fa18c504a4efd', 13, 1, 'MyApp', '[]', 0, '2018-10-15 13:07:22', '2018-10-15 13:07:22', '2019-10-15 13:07:22'),
('4a29d6a89e6ad10b615b481c1ecc4baa3bc3de95613a90dea8f0f36c801aee6191e6189b94c264b1', 13, 1, 'MyApp', '[]', 0, '2018-11-15 11:04:07', '2018-11-15 11:04:07', '2019-11-15 11:04:07'),
('4b0a445cc8b9c76d81414b60eb914ae669c6610459f135bdd55c8d20ba42df704333423f6c21f5fd', 13, 1, 'MyApp', '[]', 0, '2018-10-15 13:15:41', '2018-10-15 13:15:41', '2019-10-15 13:15:41'),
('4b6117c3c6378a9f3112ccc3e41207e8bef5ba964d8f2b9bebd7ce86c3b3764ad348570c6f497568', 5, 1, 'MyApp', '[]', 0, '2018-10-05 07:50:39', '2018-10-05 07:50:39', '2019-10-05 07:50:39'),
('4cb639bac9b24c94f58e2d8732c50a78b2e65d7a78fada86b301e8ea14626ba780093e542081515a', 2, 1, 'MyApp', '[]', 0, '2018-11-16 06:55:58', '2018-11-16 06:55:58', '2019-11-16 06:55:58'),
('515fa3f1336af2696be96425f5d9d0e230577469d105533fd6b3420bca518594b0a2132baa15e2d8', 4, 1, 'MyApp', '[]', 0, '2018-10-11 13:32:33', '2018-10-11 13:32:33', '2019-10-11 13:32:33'),
('5686000e7f42fb46bcea45db3a5a9a0de0023d947edd86de1a23605bba38df8954216d6f30046633', 2, 1, 'MyApp', '[]', 0, '2018-10-10 06:26:28', '2018-10-10 06:26:28', '2019-10-10 06:26:28'),
('57212f7f0e27f5cb55bf53a3329d3f9d727fcd33de1a5b68790089f165bd0330a366cedec05db08f', 4, 1, 'MyApp', '[]', 0, '2018-10-05 06:55:47', '2018-10-05 06:55:47', '2019-10-05 06:55:47'),
('581eb379fda8059dcdc83158230487abea2aaf3a848a8e77ff7361dec57ea8b9fc587c5db44498ca', 2, 1, 'MyApp', '[]', 0, '2018-10-11 11:54:56', '2018-10-11 11:54:56', '2019-10-11 11:54:56'),
('5aaa2c6c3bcc758a855a54798c05e70a19fe2e03709c211f23bbf2a55a932eb5bab081eec3248890', 3, 1, 'MyApp', '[]', 0, '2018-10-05 12:32:12', '2018-10-05 12:32:12', '2019-10-05 12:32:12'),
('5c138bc53501e20703df076bf5d371e1aa8bca9e7774766ed4bf6f5bd6e357a89e9f703eb79cf18f', 2, 1, 'MyApp', '[]', 0, '2018-10-10 04:57:07', '2018-10-10 04:57:07', '2019-10-10 04:57:07'),
('5d705661e293f40801dcb606e95f5b6ef8a676fec8205f16dbbed3109c8256bd21b7749d3a584fce', 2, 1, 'MyApp', '[]', 0, '2018-10-04 12:58:51', '2018-10-04 12:58:51', '2019-10-04 12:58:51'),
('5db2e0a409fc66653e2f5edea3755af3cf56e7b0d409e87d40a579cd33e403038919c3567084fbb1', 5, 1, 'MyApp', '[]', 0, '2018-10-15 07:36:27', '2018-10-15 07:36:27', '2019-10-15 07:36:27'),
('5eff4e61afee712a18ed65118d38430992988b3d47c17e4fb7c054ddd7d2f0fd3d79e839c170184b', 5, 1, 'MyApp', '[]', 0, '2018-10-04 11:39:31', '2018-10-04 11:39:31', '2019-10-04 11:39:31'),
('5f49bd0b2052f4ece00cd0e8cbeb32372108dd087687da4599933f1e7278680e1fa07bc8fc593e17', 6, 1, 'MyApp', '[]', 0, '2018-10-04 12:08:51', '2018-10-04 12:08:51', '2019-10-04 12:08:51'),
('5f9cb14298f250c4a6ac0bdfab94d613697366a43ae75440a79653681301b77471a6364ac15feb7d', 2, 1, 'MyApp', '[]', 0, '2018-10-08 05:00:00', '2018-10-08 05:00:00', '2019-10-08 05:00:00'),
('62060b57e9d2117e6352fdc2a6e31dc1a46dc62b939e256fc574befdb7a53a4f9c39476f36d09e27', 2, 1, 'MyApp', '[]', 0, '2018-10-15 06:48:09', '2018-10-15 06:48:09', '2019-10-15 06:48:09'),
('6370bcede204004ba85cb3516b6ee4dc3b1d04820cb0efa722c544030ba578c9f5f7f5615361c397', 2, 1, 'MyApp', '[]', 0, '2018-10-12 12:02:33', '2018-10-12 12:02:33', '2019-10-12 12:02:33'),
('6399d32d50c1b2e56842d0d07edc6a7db257b7dd49aabe3dc5c3d079e4c0add0c35b3a2f020afc2e', 4, 1, 'MyApp', '[]', 0, '2018-10-04 12:45:34', '2018-10-04 12:45:34', '2019-10-04 12:45:34'),
('65c444ce65fef96292e9505211511a5a08bff10d93790dd3e73a902f8101b4a24a70b3f84d6548f8', 13, 1, 'MyApp', '[]', 0, '2018-11-15 11:36:34', '2018-11-15 11:36:34', '2019-11-15 11:36:34'),
('65d1bba92b78ee3cae335a6515efc7c60b2b0074e8146ea3a4e3f590696beab884c9515b3bb93852', 6, 1, 'MyApp', '[]', 0, '2018-10-04 11:45:09', '2018-10-04 11:45:09', '2019-10-04 11:45:09'),
('6848eead3064e5477f0b3585ecbe582fbee9267f12c66e94936c891fc6d1a5fd9686ad9c2089819c', 2, 1, 'MyApp', '[]', 0, '2018-10-15 10:13:21', '2018-10-15 10:13:21', '2019-10-15 10:13:21'),
('6918aec2ab488fb2698809dad762cac2975a11894a90c0573b2349953b118e2ae7f7e4ff649e106b', 5, 1, 'MyApp', '[]', 0, '2018-10-04 12:42:37', '2018-10-04 12:42:37', '2019-10-04 12:42:37'),
('6ae1ae959a14736e11855cdab45bbf50347273f83b9391361c7506149870d3575e39edd4c4c921ca', 2, 1, 'MyApp', '[]', 0, '2018-10-05 05:39:13', '2018-10-05 05:39:13', '2019-10-05 05:39:13'),
('6f169a68b739bb17a56259d08ca728517f8a7946052d1387c9e766e81226ce71e1d54790a6376e9e', 2, 1, 'MyApp', '[]', 0, '2018-10-16 10:18:16', '2018-10-16 10:18:16', '2019-10-16 10:18:16'),
('70b52cfb92617a72b50b01997bc6d314763f849b509801bf46fdd0668bc6185a75710d34d3c9160f', 2, 1, 'MyApp', '[]', 0, '2018-10-05 11:40:13', '2018-10-05 11:40:13', '2019-10-05 11:40:13'),
('76ae31145e89686a2b679c7f589bfc874a1bcff6a78aa31cd68db24bd4baee8b15894224168d2adf', 2, 1, 'MyApp', '[]', 0, '2018-11-16 11:22:25', '2018-11-16 11:22:25', '2019-11-16 11:22:25'),
('774fd81c4914fb7ebb11dfa1af18e682322af163058cdaeec147899ffd94d2c584bfaa3b48506541', 2, 1, 'MyApp', '[]', 0, '2018-10-09 05:44:28', '2018-10-09 05:44:28', '2019-10-09 05:44:28'),
('7b13d8fc1d6d94d931cacfa215a4c807ee8db40569bfe34eebbf2e63a47227a7790ed7cd4055015a', 13, 1, 'MyApp', '[]', 0, '2018-11-15 11:24:43', '2018-11-15 11:24:43', '2019-11-15 11:24:43'),
('7b5efcfda7fb01f8b6d1fce537e6af2a4337c091dbcc48d536d18f90c3dd770f8ecd6d79eb84ae42', 4, 1, 'MyApp', '[]', 0, '2018-10-12 12:38:16', '2018-10-12 12:38:16', '2019-10-12 12:38:16'),
('7c0c0f9c43eabd8dc0cd28f6eda574c99c7b0da1f60c668d9e8846426382d6a615a856722f8c1104', 13, 1, 'MyApp', '[]', 0, '2018-11-15 11:16:26', '2018-11-15 11:16:26', '2019-11-15 11:16:26'),
('7ca5523b8a7f52b31f346b9643b81306f05e370f0ef0244a425152c7f304428ae4e68291bfa11fd6', 2, 1, 'MyApp', '[]', 0, '2018-10-11 11:08:53', '2018-10-11 11:08:53', '2019-10-11 11:08:53'),
('7ed723f828a62c6e09a303e6042d9a66144311ab5b622d4925bceb4bcd4fd26fdf9407d8fa7c1c62', 4, 1, 'MyApp', '[]', 0, '2018-10-10 06:45:46', '2018-10-10 06:45:46', '2019-10-10 06:45:46'),
('7f651afb5e80face517f1d7a44017feccc399f6dc4b6d0f5316b3c4e8af9bf7c54ecb9b10c8552be', 2, 1, 'MyApp', '[]', 0, '2018-10-16 12:39:55', '2018-10-16 12:39:55', '2019-10-16 12:39:55'),
('803426f64e4e9645116392260e300fac49c3f8440a33d0514f2be8c68b165d509cdf72b62d32ec9a', 4, 1, 'MyApp', '[]', 0, '2018-10-10 11:07:07', '2018-10-10 11:07:07', '2019-10-10 11:07:07'),
('803457062df2936569aaa14aa270a328fdbe46bac453935718b41e51646a77ed58b13f9a3c128ba2', 2, 1, 'MyApp', '[]', 0, '2018-10-05 07:06:21', '2018-10-05 07:06:21', '2019-10-05 07:06:21'),
('8057368d6d3d447c88b1bb930f2c6261c334bf9b53393b83f9702ffa6c7e61c3f9ff11707be597ec', 6, 1, 'MyApp', '[]', 0, '2018-10-04 12:10:53', '2018-10-04 12:10:53', '2019-10-04 12:10:53'),
('832d0e8de8d0972c86ccf3d3db5da543a0437ab9c4412641d2ab4734d9f10d7e6b1327239c8a8e1f', 2, 1, 'MyApp', '[]', 0, '2018-10-10 08:19:15', '2018-10-10 08:19:15', '2019-10-10 08:19:15'),
('8809e997fc8c6fcad1a1582fe3e30607453a52afd9b0bbcf4d62d42d0bff5fe8872262e08ffc1179', 2, 1, 'MyApp', '[]', 0, '2018-10-05 13:26:24', '2018-10-05 13:26:24', '2019-10-05 13:26:24'),
('88a0d9460a6b6bad98239c77cac93fb2d7b41624e540302ebaf74b6bf08c2094b08af849bb588f5c', 4, 1, 'MyApp', '[]', 0, '2018-10-11 11:31:01', '2018-10-11 11:31:01', '2019-10-11 11:31:01'),
('8947648c227a3d45af2bc545b0233580dd6421c56a846162ffe6052eb72d86c1189494af440b19a6', 5, 1, 'MyApp', '[]', 0, '2018-10-04 11:56:41', '2018-10-04 11:56:41', '2019-10-04 11:56:41'),
('8a05c1b1c0e744cfb47fe4eab4dcf07fc50690107392503d7a26572342b516c5351385a4c565ec91', 2, 1, 'MyApp', '[]', 0, '2018-11-15 05:36:34', '2018-11-15 05:36:34', '2019-11-15 05:36:34'),
('8a47b9e6ed9b2da52aac9d8af377cb8e5a191e8294a562ff7e8a22a2af3beb46bc8313a3bbc01206', 4, 1, 'MyApp', '[]', 0, '2018-10-11 11:06:00', '2018-10-11 11:06:00', '2019-10-11 11:06:00'),
('8a7b215c5e34a0fc40973b51bfa3456e3bc0db0d11ca8679da7294b43fc1c4a9c9491d771ea2d8c0', 4, 1, 'MyApp', '[]', 0, '2018-10-12 12:43:15', '2018-10-12 12:43:15', '2019-10-12 12:43:15'),
('8b1ab83241671a58c3d5d68c6499ffa67f7a4c2802972f889688d10cb48fc3bb00e04b5548f078ff', 4, 1, 'MyApp', '[]', 0, '2018-10-10 12:43:22', '2018-10-10 12:43:22', '2019-10-10 12:43:22'),
('8cdbe539930261697a0c5b914f6da310f6d4b4113bc568c21eab6b1948a19479a2347d6c1da9dac1', 4, 1, 'MyApp', '[]', 0, '2018-10-11 11:06:35', '2018-10-11 11:06:35', '2019-10-11 11:06:35'),
('8e311f75a72275257c09851315869faf85be009e248a55f09297d133c215de81b3ae3b5198eb8ef8', 13, 1, 'MyApp', '[]', 0, '2018-11-15 08:30:22', '2018-11-15 08:30:22', '2019-11-15 08:30:22'),
('921119bf946a4cfa2b9934fba8777bbfe1acf4e227729ed47b99499982ea2333a30e4bd026ff5d57', 4, 1, 'MyApp', '[]', 0, '2018-11-16 10:17:13', '2018-11-16 10:17:13', '2019-11-16 10:17:13'),
('9459f82eae30840244356345074396eff7990191e9f98d8e21864254792fbaf8a95d355396fc8ec0', 2, 1, 'MyApp', '[]', 0, '2018-10-05 10:49:40', '2018-10-05 10:49:40', '2019-10-05 10:49:40'),
('9508b9417311e96af6cf16f9a6f682855884cb639b5569bc717bfc9ff41e927e5aab62f02e043b2d', 9, 1, 'MyApp', '[]', 0, '2018-10-08 08:07:55', '2018-10-08 08:07:55', '2019-10-08 08:07:55'),
('982f66c815094db2bb891afc58ec506c7bb2aa6e239421f46687b8546441cd26a71791d422595192', 13, 1, 'MyApp', '[]', 0, '2018-11-15 11:15:28', '2018-11-15 11:15:28', '2019-11-15 11:15:28'),
('986491aa7a346aa8a284bbeb42d91533f7256fa446862eca6f26cdc743ef7200f92aa011352085e3', 4, 1, 'MyApp', '[]', 0, '2018-10-11 12:17:32', '2018-10-11 12:17:32', '2019-10-11 12:17:32'),
('9a72fb0fbab3e4a6587fa089c01433ab71b681d9971488173cc93abec24e083f542a5060ddc94b1b', 2, 1, 'MyApp', '[]', 0, '2018-10-10 06:25:53', '2018-10-10 06:25:53', '2019-10-10 06:25:53'),
('9aebf8c5cf16e6f17bf5496bfab58da16979ffa2fbb1c460c0b759c47de3acce3bbca377d9d322d4', 4, 1, 'MyApp', '[]', 0, '2018-10-15 07:26:11', '2018-10-15 07:26:11', '2019-10-15 07:26:11'),
('9b5a654111ed3200f39bca75dd26067b637b4c7566cf966bce3a016c27c0363d603e1bb7501a457a', 4, 1, 'MyApp', '[]', 0, '2018-10-10 06:26:08', '2018-10-10 06:26:08', '2019-10-10 06:26:08'),
('9b8716206a2698dc500cc5054ddccf36ffd20eadc1b3d339d944b7e7e54d96eaed206e5f97cea228', 13, 1, 'MyApp', '[]', 0, '2018-11-15 10:55:49', '2018-11-15 10:55:49', '2019-11-15 10:55:49'),
('9cb72b257bb281fc858969e2059aeddd6b1fa4cafcea003ce66446356abe82073485fe507f1f4d9c', 5, 1, 'MyApp', '[]', 0, '2018-10-15 07:42:05', '2018-10-15 07:42:05', '2019-10-15 07:42:05'),
('a081d8d1e0d070621b46ed332046d3de6a252952fb9f2afbed63b0fdcf20619786fa81e16f994fe7', 2, 1, 'MyApp', '[]', 0, '2018-10-16 12:52:57', '2018-10-16 12:52:57', '2019-10-16 12:52:57'),
('a11da8bf19be12516d195941778ba8ac054ef0fd317553a799df0e2dd9435868b74d0cd00fd06adc', 4, 1, 'MyApp', '[]', 0, '2018-10-15 07:10:14', '2018-10-15 07:10:14', '2019-10-15 07:10:14'),
('a1a17ec480c08c00294c62822057676094e39967cefcd4b7c1ccc7c7d33e5588755aba9363741e1d', 2, 1, 'MyApp', '[]', 0, '2018-10-05 10:23:46', '2018-10-05 10:23:46', '2019-10-05 10:23:46'),
('a3718f5b43be9bcad994422ddae7bdcfc1578db338b982d8380f2d8a17e8d6a22fa5b94968b52820', 6, 1, 'MyApp', '[]', 0, '2018-10-04 12:06:58', '2018-10-04 12:06:58', '2019-10-04 12:06:58'),
('a710a2a656c32a7985de444f5293c5aae0c373ab11b48b07688fee17140f171ce2b6cf32b0d49c13', 4, 1, 'MyApp', '[]', 0, '2018-10-04 12:20:09', '2018-10-04 12:20:09', '2019-10-04 12:20:09'),
('a83e993e5619c90fb06c8fae1d301e380a55d0df8582fcac50cb4343016b52a45fb76ffdff24331e', 9, 1, 'MyApp', '[]', 0, '2018-10-08 08:08:56', '2018-10-08 08:08:56', '2019-10-08 08:08:56'),
('aae594024f59c4ad02994d84f28202ab32c59872c70eedfd67bfa70bd2010d627b8ec0189faccf4e', 3, 1, 'MyApp', '[]', 0, '2018-10-04 12:56:13', '2018-10-04 12:56:13', '2019-10-04 12:56:13'),
('ac794b94c98aff6e2d271de18eb3a9b2bec3ff04e7b7a91898b5d362c07b9ad0171ea951c1519879', 2, 1, 'MyApp', '[]', 0, '2018-10-05 06:58:58', '2018-10-05 06:58:58', '2019-10-05 06:58:58'),
('b291d26c24de678bbce61f17661dd1efaccf50b75c446adf48182a4418595575e76162b4ed890822', 4, 1, 'MyApp', '[]', 0, '2018-10-05 07:22:43', '2018-10-05 07:22:43', '2019-10-05 07:22:43'),
('b4a4bb978dae7febb5d011abdcb658b473b1030f78db3ff3f2e35ced9b2741ca21e274980fb05e9f', 5, 1, 'MyApp', '[]', 0, '2018-10-12 11:58:30', '2018-10-12 11:58:30', '2019-10-12 11:58:30'),
('b628a41ff8848fad725d1a6c73db1a725ca6462ad3a11fd124e99c7e12f0e5cb0578263476853434', 13, 1, 'MyApp', '[]', 0, '2018-11-15 11:37:24', '2018-11-15 11:37:24', '2019-11-15 11:37:24'),
('b6a041b9982efac35e2a37209a4536f20eda9c0896d7cce26976a9f2ad7f5f44c0ac4a753c6f594c', 2, 1, 'MyApp', '[]', 0, '2018-10-04 11:40:29', '2018-10-04 11:40:29', '2019-10-04 11:40:29'),
('b6d17aff8f2ee19b584a6c7022bb07a4918d6c6b2cbd2e470568e9a2709e85017cdd4f282d90073b', 4, 1, 'MyApp', '[]', 0, '2018-10-11 11:32:06', '2018-10-11 11:32:06', '2019-10-11 11:32:06'),
('b7620841c353a1bf76cca6bcc1e52be939232c2eadf6607d5c61781224281890595a16477ff9dbef', 3, 1, 'MyApp', '[]', 0, '2018-10-04 11:50:32', '2018-10-04 11:50:32', '2019-10-04 11:50:32'),
('b9ddc9c2e5d64dde6345a2c971dfffee1ee39668d4be3575c1cf8b10a215a95063e49a3144134d8e', 5, 1, 'MyApp', '[]', 0, '2018-11-16 11:21:17', '2018-11-16 11:21:17', '2019-11-16 11:21:17'),
('ba34c26884b5c9e3f45d39bf3acdce5287e93399bb149ba43999119b5c94633ad15e4e23078edca0', 13, 1, 'MyApp', '[]', 0, '2018-11-15 11:35:41', '2018-11-15 11:35:41', '2019-11-15 11:35:41'),
('ba87a39214b08d53101cc471dfb09ee0ef69e8c981cfd1178182a24c7f8bc8b4f7776d76898bd028', 4, 1, 'MyApp', '[]', 0, '2018-10-12 05:02:16', '2018-10-12 05:02:16', '2019-10-12 05:02:16'),
('bb3d06b7866140201673e06ed2799a2eb4afda912e857c3848a6c49a2630865c804475cf4a561f31', 5, 1, 'MyApp', '[]', 0, '2018-10-11 11:10:45', '2018-10-11 11:10:45', '2019-10-11 11:10:45'),
('bb8c8cc6637b7ead80f25f9f77d135a0dca02b967efff370e49017e79307bb2bdb87f6b8bc937f1b', 6, 1, 'MyApp', '[]', 0, '2018-10-04 12:07:43', '2018-10-04 12:07:43', '2019-10-04 12:07:43'),
('be31c3a39f3280e2ec8570d7ed1c3b74a314aab718cdb3fbb2243a0e4455a3b97d6df794a22353e5', 3, 1, 'MyApp', '[]', 0, '2018-10-08 08:06:10', '2018-10-08 08:06:10', '2019-10-08 08:06:10'),
('c08efec607f1de789556d30dbc0d7f6fc0c827eef416de6de54233f4f189e035e20132655e258024', 13, 1, 'MyApp', '[]', 0, '2018-11-15 11:01:36', '2018-11-15 11:01:36', '2019-11-15 11:01:36'),
('c0a798e11d43f2a6ddd20ed6ed211177f29e6677b5cbf71c0c70db82ab8be3279f8f65f6853a07f2', 4, 1, 'MyApp', '[]', 0, '2018-10-10 12:55:45', '2018-10-10 12:55:45', '2019-10-10 12:55:45'),
('c0d0e890e1c9dbc9145d1056e00634584217237bf38245746f0b275f47058d22d91f462b8a07c37e', 4, 1, 'MyApp', '[]', 0, '2018-10-12 11:59:14', '2018-10-12 11:59:14', '2019-10-12 11:59:14'),
('c38c30d550e9ea1be173c9ec7a0df931bb1c5ef7e8a419f2a0d895299ac340f77914410d127ae3d5', 3, 1, 'MyApp', '[]', 0, '2018-10-04 11:27:42', '2018-10-04 11:27:42', '2019-10-04 11:27:42'),
('c67a01190898bf73ba2e5e10d910352f5c16a635ad02a832088baad5534f652360dcbd72b3b83748', 4, 1, 'MyApp', '[]', 0, '2018-10-15 07:29:38', '2018-10-15 07:29:38', '2019-10-15 07:29:38'),
('c8dd5fb19d41b07fc2c0ffc8136ef81f851ebbd9e22445ffc16783b9737909712947bef7e2d9e88b', 3, 1, 'MyApp', '[]', 0, '2018-10-10 07:27:39', '2018-10-10 07:27:39', '2019-10-10 07:27:39'),
('c9f2f8e49f7b15dbd8ec7a14834db017104ef2c4e603ffad99bcb5e4812c79203a7d501f0dbe590b', 2, 1, 'MyApp', '[]', 0, '2018-10-16 12:53:52', '2018-10-16 12:53:52', '2019-10-16 12:53:52'),
('cb572f8b692079ea792708ca33e0152b72e3546fcec97924d2b57eb1b9ff752be56104932240e12f', 4, 1, 'MyApp', '[]', 0, '2018-10-04 11:49:57', '2018-10-04 11:49:57', '2019-10-04 11:49:57'),
('cbb67f69fabbec802dde1c0589d2d9cb7cf8ddd4b100b38598c37107b4f1091d09dc13c8abe9974e', 4, 1, 'MyApp', '[]', 0, '2018-11-15 05:37:23', '2018-11-15 05:37:23', '2019-11-15 05:37:23'),
('ccd2fe89b6be6a23c44a14f41eef7dc317fa804487753858e508424f6f9615ac365a53009de126e0', 13, 1, 'MyApp', '[]', 0, '2018-11-15 08:25:09', '2018-11-15 08:25:09', '2019-11-15 08:25:09'),
('ced042b52879004183aca7cb336b5af6f97793e6709264eaa3f9ca3cddbbc0a357fc80a2d1a5fc5f', 4, 1, 'MyApp', '[]', 0, '2018-10-10 06:39:31', '2018-10-10 06:39:31', '2019-10-10 06:39:31'),
('d031f05f313eaa249090bf63d742dae7fb22dbcd97cfaa2a99390851f8f4b1cfb63e6945af4b6bb9', 2, 1, 'MyApp', '[]', 0, '2018-11-16 05:47:43', '2018-11-16 05:47:43', '2019-11-16 05:47:43'),
('d114e7fbb669ec8e40e2c6c98c10b66c4f697d447a504e7acc7fe9b073412ae64099bd88799241f6', 5, 1, 'MyApp', '[]', 0, '2018-10-11 11:09:35', '2018-10-11 11:09:35', '2019-10-11 11:09:35'),
('d252f3fd39fb25b603d075c0709656ed24f57adb38be9d1e4291b1d0cfb4caf15b4d78bd73f87e3e', 2, 1, 'MyApp', '[]', 0, '2018-11-16 10:17:25', '2018-11-16 10:17:25', '2019-11-16 10:17:25'),
('d806d27465921f6b4676bd7edbab547724704d9f64cb6058e346a9fc4471aad99405ce8dfa27d35c', 4, 1, 'MyApp', '[]', 0, '2018-10-04 12:21:43', '2018-10-04 12:21:43', '2019-10-04 12:21:43'),
('d992845170a605f3de413dcdf4d1943b5c118776fc5a252cf4597a81efa2531297694ddadc8c8aae', 4, 1, 'MyApp', '[]', 0, '2018-10-10 04:56:45', '2018-10-10 04:56:45', '2019-10-10 04:56:45'),
('daafbe420e50e228f4be010c8ff5901f49cae2f99c0f96a2fc9b87188364eb172b696c5ff0442ab4', 5, 1, 'MyApp', '[]', 0, '2018-10-15 07:38:26', '2018-10-15 07:38:26', '2019-10-15 07:38:26'),
('dc35ec03374eaec18f21cb7a4d7068f69d5419e132ccefce1e63f3f396b753ceaa1504a435181d88', 4, 1, 'MyApp', '[]', 0, '2018-10-12 05:03:57', '2018-10-12 05:03:57', '2019-10-12 05:03:57'),
('dd6ff90e4739db3dbce74ba0e9c56c18dbadb6f2f59ad2a7bf59762103e221a4fbf27c45b4fe7877', 4, 1, 'MyApp', '[]', 0, '2018-10-16 07:18:59', '2018-10-16 07:18:59', '2019-10-16 07:18:59'),
('df85c00b187fe98e949e297622891ef297a44e4aa8b2f2c932cb71afb6ed8c7b0c2fc2ebcd792b7d', 2, 1, 'MyApp', '[]', 0, '2018-11-16 09:50:38', '2018-11-16 09:50:38', '2019-11-16 09:50:38'),
('e2431b7ebc28b5327c8e411304239cf8428127d3c77eb018ddc1a55c4e64aeec4c079738a85bb395', 2, 1, 'MyApp', '[]', 0, '2018-10-09 05:02:41', '2018-10-09 05:02:41', '2019-10-09 05:02:41'),
('e36ea185f21708748da0b640d470732cb83fbe357dd6433e0b7a23e37f45345c237182877b5f55ea', 4, 1, 'MyApp', '[]', 0, '2018-10-04 12:57:05', '2018-10-04 12:57:05', '2019-10-04 12:57:05'),
('e3c2fc40657aacce128da5d5c9fc2032c4fd781df990a6e1a6938168d8f24ff82a34af2d5cc5758d', 2, 1, 'MyApp', '[]', 0, '2018-10-11 11:06:10', '2018-10-11 11:06:10', '2019-10-11 11:06:10'),
('e4ace47584c40395a073dbdac6c32672b5647e002b7e90ccb7133db768f0174ed838aa21cddaa290', 4, 1, 'MyApp', '[]', 0, '2018-10-15 10:21:40', '2018-10-15 10:21:40', '2019-10-15 10:21:40'),
('e5420c03e975da048e3fb9e4e7a60fa4176ccbc47c382c49099646bf133afd31cd0a483861b65b11', 4, 1, 'MyApp', '[]', 0, '2018-11-16 05:08:16', '2018-11-16 05:08:16', '2019-11-16 05:08:16'),
('e58f4be78f3d7ca78d907350f892130bac2cc0398f0d3daa919cf4b500e2f379ab8b7854e13405e9', 2, 1, 'MyApp', '[]', 0, '2018-10-10 05:47:20', '2018-10-10 05:47:20', '2019-10-10 05:47:20'),
('e7ccc0f5898e475215ca995b6475bce770c086ab9fd986a70314c6b50389c79a9d5634668484b706', 2, 1, 'MyApp', '[]', 0, '2018-10-09 09:44:30', '2018-10-09 09:44:30', '2019-10-09 09:44:30'),
('e82dac231628b9cf748925dda648ca16d47bc0cb849d50026c5efa2de71a9f11d9d4f15863d60298', 5, 1, 'MyApp', '[]', 0, '2018-10-15 07:40:36', '2018-10-15 07:40:36', '2019-10-15 07:40:36'),
('e89cb5a1a4c87dde9e830df9c9108750872566a23bfc13845bb37d52c29e1466f9d64c5779bda666', 4, 1, 'MyApp', '[]', 0, '2018-10-04 12:44:41', '2018-10-04 12:44:41', '2019-10-04 12:44:41'),
('eaf359daa9f58359b728ff6bd6541778bfdccea8ade4f20e4027d2921fd9bbd579f94f29f7422970', 5, 1, 'MyApp', '[]', 0, '2018-10-04 12:43:41', '2018-10-04 12:43:41', '2019-10-04 12:43:41'),
('ef0072667c579d699adcef30f7f869ea2433f2485df6d93efaec09ab3bcd2ff3bf69a80a419e9845', 13, 1, 'MyApp', '[]', 0, '2018-11-15 10:58:23', '2018-11-15 10:58:23', '2019-11-15 10:58:23'),
('ef1bf370de294d1a7f013b4f95b25e04f4b4236a1ee790be1748b0c56788f53d18709de8e6fdd043', 4, 1, 'MyApp', '[]', 0, '2018-10-09 05:40:48', '2018-10-09 05:40:48', '2019-10-09 05:40:48'),
('f295479ee506b49d9184c9b05b6bbbf33d45cde1cd88d65620d697adde908cc9e1f02377aa853139', 13, 1, 'MyApp', '[]', 0, '2018-11-15 08:28:15', '2018-11-15 08:28:15', '2019-11-15 08:28:15'),
('f2b64ee8ad2a65419cc93c7679755b4b967c05afab66cf1826168287330b73586b76c2a698ad8b15', 2, 1, 'MyApp', '[]', 0, '2018-10-04 12:45:13', '2018-10-04 12:45:13', '2019-10-04 12:45:13'),
('f40fedaa250ad9f8b80253096cb9517cd6aa33549de32032347413cfacc009c177759e3902e2e509', 3, 1, 'MyApp', '[]', 0, '2018-10-11 11:31:36', '2018-10-11 11:31:36', '2019-10-11 11:31:36'),
('f53f24fa28cce7ed18c5cbaf69b94762eaee3fb61146d90f03f16d22f68e544e415231038b8b7713', 2, 1, 'MyApp', '[]', 0, '2018-10-04 12:50:55', '2018-10-04 12:50:55', '2019-10-04 12:50:55'),
('f5ea05b8cf738e0033752c1269bd72bdb32e13b4c46f48201e46c56599ee0e6a342e94565eff765b', 4, 1, 'MyApp', '[]', 0, '2018-10-11 11:37:00', '2018-10-11 11:37:00', '2019-10-11 11:37:00'),
('f647e08fe62ce0a79beeea7f0140ee48fbddc624413aca2896fc32f858d704562455427d2db6754e', 4, 1, 'MyApp', '[]', 0, '2018-10-15 07:36:03', '2018-10-15 07:36:03', '2019-10-15 07:36:03'),
('f65036575f4ea1182da2932a4975092c9da56669b917e0675d98f30697e8076f752d1a90ce0a3403', 13, 1, 'MyApp', '[]', 0, '2018-10-15 13:17:32', '2018-10-15 13:17:32', '2019-10-15 13:17:32'),
('f77aa9eabc9ae1ddb2501228e5031fc03c038b3b8d7e81410f776463906e5ecedef3021b11cbbb28', 13, 1, 'MyApp', '[]', 0, '2018-10-15 13:12:53', '2018-10-15 13:12:53', '2019-10-15 13:12:53'),
('f83d052a0dd11a6af857f19ef18a1bb810b2398e8c077a0ea6b83fa82bea01ca53cbba40a46fd208', 2, 1, 'MyApp', '[]', 0, '2018-11-15 05:37:32', '2018-11-15 05:37:32', '2019-11-15 05:37:32'),
('f871bc24fc071a1bc0795809e495084810b26361982d3b7d6e60f17876bb98050c16889dba5e4fc5', 2, 1, 'MyApp', '[]', 0, '2018-10-05 10:53:43', '2018-10-05 10:53:43', '2019-10-05 10:53:43'),
('f88fb7e28951a2ae0eacf8b0becefd692decbf0a0590e03c18d1c8a594c2cfad664a4dd422b5e8bc', 4, 1, 'MyApp', '[]', 0, '2018-10-08 08:12:40', '2018-10-08 08:12:40', '2019-10-08 08:12:40'),
('f8c47b1e9bdc6ee8d8ece3fa4f79badd2f1403b557e1c520880e6b1ed2533e8b10899924c7b3cf4d', 2, 1, 'MyApp', '[]', 0, '2018-10-19 05:09:28', '2018-10-19 05:09:28', '2019-10-19 05:09:28'),
('fc38273f3a3185df13dc9767dea9d81d5ef54d3fe97708c6cbfd2fea3939a0deb07955648773bc14', 13, 1, 'MyApp', '[]', 0, '2018-10-15 13:13:54', '2018-10-15 13:13:54', '2019-10-15 13:13:54'),
('fef531d16fae99006b339d6afad503b02d16e9fb60052c904e395bf32566d87cd671a35629c7f527', 2, 1, 'MyApp', '[]', 0, '2018-10-04 11:25:30', '2018-10-04 11:25:30', '2019-10-04 11:25:30'),
('ffdfd144072f05f15a9a32ccb90f163f0773ab2b1e61fd5a78e4073ff55b8bf48d1e4692db6bcda7', 5, 1, 'MyApp', '[]', 0, '2018-10-05 05:01:55', '2018-10-05 05:01:55', '2019-10-05 05:01:55'),
('ffe879b1615cabedbb20c80373d5d34ad532028d68900a850fd1a2a55e177ba624864c6db2af5d10', 4, 1, 'MyApp', '[]', 0, '2018-10-12 13:34:06', '2018-10-12 13:34:06', '2019-10-12 13:34:06');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', 'szZlkmQklQdSMK4rFQrXpDeXH39FGcfeqJeMQBqs', 'http://localhost', 1, 0, 0, '2018-10-04 11:12:02', '2018-10-04 11:12:02'),
(2, NULL, 'Laravel Password Grant Client', '4fwIIEBRnhJMN14Y4rCgzQR6i15J7jL1lhGQUJ40', 'http://localhost', 0, 1, 0, '2018-10-04 11:12:02', '2018-10-04 11:12:02');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2018-10-04 11:12:02', '2018-10-04 11:12:02');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `project-assign-user`
--

CREATE TABLE `project-assign-user` (
  `id` int(10) UNSIGNED NOT NULL,
  `project_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `project-assign-user`
--

INSERT INTO `project-assign-user` (`id`, `project_id`, `user_id`, `user_status`, `created_at`, `updated_at`) VALUES
(25, '5', '6536445435564356', 1, '2018-11-15 05:45:03', '2018-11-15 05:45:03'),
(31, '2', '1538653239958', 1, '2018-11-16 07:36:48', '2018-11-16 07:36:48'),
(35, '4', '1538652482548', 1, '2018-11-16 11:28:01', '2018-11-16 11:28:01'),
(36, '4', '1538653239958', 1, '2018-11-16 11:28:01', '2018-11-16 11:28:01'),
(37, '4', '6536445435564356', 1, '2018-11-16 11:32:25', '2018-11-16 11:32:25'),
(40, '3', '1538652947574', 1, '2018-11-16 11:57:41', '2018-11-16 11:57:41');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_no` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `approve_user` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `user_id`, `email`, `password`, `phone_no`, `role_id`, `status`, `approve_user`, `remember_token`, `created_at`, `updated_at`) VALUES
(2, '1538650932588', 'hr@gmail.com', '$2y$10$YxyiKQOLN62bQbtTj/gnbulKyzajkXPczd3pY181Z8xDV27pm38oG', '9711280827', '2', '0', '1', NULL, '2018-10-04 11:14:33', '2018-11-15 12:12:24'),
(3, '1538652055873', 'hr@cloud.com', '$2y$10$qVPqk5DmH1PKAUB5.oDCbuQ9OTOxDk3242viQ80MPe6Tn9SJxFnxm', '9536063146', '2', '0', '1', NULL, '2018-10-04 11:21:57', '2018-10-15 10:18:14'),
(4, '1538652482548', 'aakankshi@gmail.com', '$2y$10$P9cydEhg9S1PTXQo0osPw.58qewq6Z101HMf18F2r6EqF.UnseALu', '9767767656', '1', '1', '1', NULL, '2018-10-04 11:28:48', '2018-11-16 08:09:35'),
(5, '1538652947574', 'zuvair@gmail.com', '$2y$10$fP.1f2kcjf5cQswFSdUjSOnU6puIv9GdvTbBXFfH21c/zTX8NsdEW', '7767667664', '1', '1', '1', NULL, '2018-10-04 11:36:06', '2018-10-11 11:31:56'),
(6, '1538653239958', 'harry@gmail.com', '$2y$10$JAwb5PhZ0K.DV1wjQeHzM..NHe.xvI/xircb4skZoiqOW9uAaa9ba', '775305766', '1', '1', '0', NULL, '2018-10-04 11:42:16', '2018-11-15 05:45:35'),
(7, '1538653843873', 'Ramu@gmail.com', '$2y$10$FFk0KWbgOnWWaTd9wwK83OSO9KBwY5D5DeBQeJ8oLDLUG.WxO4J4C', '56547675667', '1', '1', '1', NULL, '2018-10-04 11:53:02', '2018-11-16 11:57:06'),
(8, '1538660605341', 'ritu@gmail.com', '$2y$10$/cWge8eXxhlLzR895DYN/.FjQ3sZYJDGGxIZrUax5.Ghf.jSAXn9q', '9711280827', '1', '1', '1', NULL, '2018-10-04 13:44:20', '2018-10-16 11:02:01'),
(12, '1539341787244', 'cuminte@gmail.com', '$2y$10$HW1UYwYrowYoRf76M3VVGuxGdpF0YtKLWgS55Nxgh2tPO1QAWtjrm', '775305977444', '1', '0', '0', NULL, '2018-10-12 11:57:31', '2018-11-15 10:08:11'),
(13, '34753655656456', 'admin@gmail.com', '$2y$10$YxyiKQOLN62bQbtTj/gnbulKyzajkXPczd3pY181Z8xDV27pm38oG', '7753059779', '3', '0', '0', NULL, '2018-10-15 13:06:36', '2018-10-15 13:06:36'),
(22, '1542276671169', '7753059779@gmail.com', '$2y$10$NbZXUBWIxMrM.ZxzqkV36u.bvoYg8DyUjim7H9mbtkRUzrmmTtCN6', '9711280827', '1', '1', '0', NULL, '2018-11-15 10:11:43', '2018-11-15 10:11:43'),
(23, '1542355483161', '9711280827', '$2y$10$6VaKG1XqdEiKKf2k9zjp3.hpE7vTrjrx25pYlF3mQ3qlAj3ybtG/.', 'JKJKLJKL', '1', '1', '0', NULL, '2018-11-16 08:05:03', '2018-11-16 08:05:07'),
(24, '1542356406817', 'ritu45@gmail.com', '$2y$10$kUzE89o99nsb7axcr8frv.s8cTPqBwUT7D73SJZkMPiKNEm0RMZ1W', '7766767676', '2', '0', '0', NULL, '2018-11-16 08:20:31', '2018-11-16 08:20:31'),
(25, '1542368507733', 'testing@gmail.com', '$2y$10$.c4E08.51YL.pbO.a2sXnesnif38rSqubxUu7oSVk/Bm6yaHogugW', '7753059779', '1', '1', '0', NULL, '2018-11-16 11:42:31', '2018-11-16 11:57:30'),
(26, '1542368571351', 'test2@gmail.com', '$2y$10$80MmLRY/edpQ.2MZ2326reKDvhCNbhRNzXb0Pk1TASE4uWo5aadhe', '7753059779', '1', '1', '0', NULL, '2018-11-16 11:43:41', '2018-11-16 11:57:31');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_slide_show`
--
ALTER TABLE `admin_slide_show`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_testimonials`
--
ALTER TABLE `admin_testimonials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_top_ranks`
--
ALTER TABLE `admin_top_ranks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company_jobs`
--
ALTER TABLE `company_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company_projects`
--
ALTER TABLE `company_projects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `com_business_details`
--
ALTER TABLE `com_business_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `com_director_details`
--
ALTER TABLE `com_director_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `com_infos`
--
ALTER TABLE `com_infos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `emp_educations`
--
ALTER TABLE `emp_educations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `emp_experiences`
--
ALTER TABLE `emp_experiences`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `emp_personal_details`
--
ALTER TABLE `emp_personal_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `emp_skills`
--
ALTER TABLE `emp_skills`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_personal_access_clients_client_id_index` (`client_id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `project-assign-user`
--
ALTER TABLE `project-assign-user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_slide_show`
--
ALTER TABLE `admin_slide_show`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `admin_testimonials`
--
ALTER TABLE `admin_testimonials`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `admin_top_ranks`
--
ALTER TABLE `admin_top_ranks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `blogs`
--
ALTER TABLE `blogs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `company_jobs`
--
ALTER TABLE `company_jobs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `company_projects`
--
ALTER TABLE `company_projects`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `com_business_details`
--
ALTER TABLE `com_business_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `com_director_details`
--
ALTER TABLE `com_director_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `com_infos`
--
ALTER TABLE `com_infos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `emp_educations`
--
ALTER TABLE `emp_educations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `emp_experiences`
--
ALTER TABLE `emp_experiences`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `emp_personal_details`
--
ALTER TABLE `emp_personal_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `emp_skills`
--
ALTER TABLE `emp_skills`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `project-assign-user`
--
ALTER TABLE `project-assign-user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
