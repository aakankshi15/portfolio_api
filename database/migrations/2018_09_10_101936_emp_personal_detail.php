<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EmpPersonalDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		 Schema::create('emp_personal_details', function (Blueprint $table) {
            $table->increments('id');
			$table->string('user_id');
            $table->string('employee_name');
            $table->string('current_company');
			$table->string('company_id')->nullable();
			$table->string('position')->nullable();
			$table->string('date_of_joining');
			$table->string('employee_dob');
			$table->string('employee_pincode');
            $table->string('contact_num');
            $table->string('employee_address');
			$table->string('employee_gender');
			$table->string('employee_image')->nullable();
			$table->string('back_image')->nullable();
			$table->string('employee_resume')->nullable();
           $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
           $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
		   });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
          Schema::dropIfExists('emp_personal_details');
    }
}
