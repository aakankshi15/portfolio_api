<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ComInformation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('com_infos', function (Blueprint $table) {
            $table->increments('id');
			$table->string('company_user_id');
            $table->string('company_name');
            $table->string('company_id');
			 $table->string('company_url');
            $table->string('company_logo');
			$table->string('back_image')->nullable();
			 $table->string('country');
            $table->string('state');
			 $table->string('city');
			  $table->string('postal_code');
			 
			
           $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
           $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
		   });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('com_infos');
    }
}
