<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EmpExperience extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('emp_experiences', function (Blueprint $table) {
            $table->increments('id');
			$table->string('user_id');
			$table->string('company_Name');
            $table->string('employee_id');
            $table->string('position');
			 $table->string('time_prefered');
            $table->string('work_exp_year');
			 $table->string('work_exp_month');
            $table->string('date_of_joining');
			 $table->string('date_of_closing');
			
           $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
           $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
		   });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists('emp_experiences');
    }
}
