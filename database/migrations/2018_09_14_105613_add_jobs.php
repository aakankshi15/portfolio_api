<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddJobs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_jobs', function (Blueprint $table) {
            $table->increments('id');
			$table->string('company_id');
            $table->string('category');
            $table->string('designation');
			$table->string('description');
			$table->string('eligibility');
			$table->string('location');
			$table->string('skill_required');
			$table->string('salary');
			$table->string('experience');
           $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
           $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
		   });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists('company_jobs');
    }
}
