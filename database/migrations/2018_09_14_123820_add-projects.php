<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProjects extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('company_projects', function (Blueprint $table) {
            $table->increments('id');
			$table->string('company_id');
            $table->string('project_name');
			$table->string('project_description');
			$table->string('project_images');
           $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
           $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
		   });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_projects');
    }
}
